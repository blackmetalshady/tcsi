<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;
use Cake\I18n\Time;

/**
 * Users Controller
 *
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public $paginate = [
        'limit' => 25,
        'order' => [
            'Users.firstname' => 'asc'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['register','resetpassword', 'newslettersubscription','test']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /*public function updateDB(){
        $this->loadModel('Countries');
        $countries = $this->Countries->find();

        foreach ($countries as $country) {
            echo $country->name.'<br/>';
            $res = $this->Users->updateAll(
                ['country' => $country->id],
                ['country' => $country->name]);
        }
        echo "kolchi 5/5"; die();
    }*/
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function register(){
        if ($this->request->is('post')) {
            //print_r($this->request->getData()); die();
            if($this->Users->exists(["primaryemail"=>$this->request->getData()['primaryemail']])){
                echo "exists";die();
            }

            if (empty($this->request->getData()['category_id'])) {
                $this->loadModel('Categories');
                $this->request->getData()['category_id'] = $this->Categories->find()->where(['name LIKE' => '%press%'])->first()->id;
            }
            $user = $this->Users->newEntity();
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if($usr = $this->Users->save($user)){
                if (isset($this->request->getData()['gallerie']) && !empty($this->request->getData()['gallerie'])) {
                    //////////////////////////////////////// Gallerie Upload
                    $this->loadModel("Pictures");
                    if (!file_exists(WWW_ROOT.'img/gallerie/'.$usr->id) || !is_dir(WWW_ROOT.'img/gallerie/'.$usr->id)) {
                        mkdir(WWW_ROOT.'img/gallerie/'.$usr->id, 0777);
                    }
                    foreach ($this->request->getData()['gallerie'] as $pic) {
                        //print_r($pic); die();
                        if (!empty($pic)) {
                            $ext = strtolower(pathinfo($pic['name'], PATHINFO_EXTENSION));
                            $filename = md5($usr->id.time()).'.'.$ext;
                            $finalname = WWW_ROOT.'img/gallerie/'.$usr->id.'/'.$filename;
                            if(move_uploaded_file($pic['tmp_name'], $finalname)){
                                chmod($finalname, 0766);
                                $photo = $this->Pictures->newEntity();
                                $photo = $this->Pictures->patchEntity($photo, ['user_id'=>$usr->id, 'filename'=>$filename]) ;
                                if (!$this->Pictures->save($photo)) {
                                    print_r($photo); die();
                                }
                            }else{
                                print_r($photo); die();
                            }
                        }
                    }
                    //$this->sendadminemail($this->request->getData(), $usr->id);
                    if ($this->sendemail($user->primaryemail)) {
                        $this->sendadminemail($this->request->getData(), $usr->id);
                    }else{
                        echo "success";die();
                        //echo "error send email"; die();
                    }
                    echo "success";die();
                }else{
                    if ($this->sendemail($user->primaryemail)) {
                        $this->sendadminemail($this->request->getData(), $usr->id);
                    }else{
                        echo "success";die();
                        //echo "error send email"; die();
                    }
                    echo "success";die();
                }
            }else{
                echo "Error saving user data\n";
                print_r($user); die();
            }
            //print_r($this->request->getData()); die();
        }else{
            echo "Get Method is not allowed";die();
        }

        die();
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Users->find()->where(['primaryemail'=>$this->request->getData()['primaryemail']])->first();
            if (!empty($user)) {
                if (empty($user->password)) {
                    $this->request->session()->write("Usermail", $user['primaryemail']);
                    echo "reset";die();
                    //$this->redirect("/users/resetpassword");
                }else{
                    $user = $this->Auth->identify();
                    if ($user) {
                        $this->Auth->setUser($user);
                        echo "success"; die();
                        //return $this->redirect($this->Auth->redirectUrl());
                    } else {
                        echo "fail"; die();
                        //$this->Flash->error(__("Nom d'utilisateur ou mot de passe incorrect"));
                    }
                }
            }else{
                echo "fail";die();
            }
            
        }
    }

    public function profile($user_id=null){
        $session_id = $this->request->session()->read('Auth.User.id');
        if (isset($user_id)) {
            $user = $this->Users->find()->where(['Users.id'=>$user_id])->contain(['Categories','Pictures'])->first();
            if (empty($user)) {
               throw new NotFoundException(__('User Not Found'));
            }
        }else{
            $user = $this->Users->find()->where(['Users.id'=>$this->request->session()->read('Auth.User.id')])
                    ->contain([
                        'Categories', 
                        'Pictures', 
                        'Payments' => function ($q) {
                            return $q->where([
                                        'Payments.create_time <' => date('Y-m-d H:i:s'), 
                                        'Payments.due_time >' => date('Y-m-d H:i:s')
                                    ]);
                        }
                    ])
                    ->first();

            if ($user->isfree == '0' && $user->type != 'press') {
                if (empty($user->payments)) {
                    return $this->redirect(['controller'=>'Payments','action' => 'proceed']);
                }
            }
        }
        

        $this->set(compact('user','session_id'));
    }

    public function resetpassword(){
        if (empty($this->request->session()->read('Usermail'))) {
            return $this->redirect(['controller'=>'Pages','action' => 'display','home']);
        }
    }

    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    public function newslettersubscription(){
        if ($this->request->is('post')) {
            $this->loadModel("Newsletters");
            if($this->Newsletters->exists(["email"=>$this->request->getData()['email']]) || $this->Users->exists(["primaryemail"=>$this->request->getData()['email']])){
                echo "exists";die();
            }else{
                $nl = $this->Newsletters->newEntity();
                $nl = $this->Newsletters->patchEntity($nl, $this->request->getData());
                if ($this->Newsletters->save($nl)) {
                    echo "success"; die();
                }else{
                    //print_r($nl); die();
                    echo "fail"; die();
                }
            }
            
        }
    }
    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $id = (empty($id))? $this->request->session()->read('Auth.User.id') : $id;

        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        if(isset($this->request->getData()['primaryemail']) && $this->Users->exists(["primaryemail"=>$this->request->getData()['primaryemail']]) && $this->request->getData()['primaryemail'] != $this->request->session()->read('Auth.User.primaryemail')){
                echo "exists";die();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                echo "success";
                die();
                //$this->Flash->success(__('The user has been saved.'));

                //return $this->redirect(['action' => 'index']);
            }
            echo "fail"; die();
            //$this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function search($country=null, $category=null){
    	$user = $this->Users->find()->where(['Users.id'=>$this->request->session()->read('Auth.User.id')])
                ->contain([
                    'Payments' => function ($q) {
                        return $q->where([
                                    'Payments.create_time <' => date('Y-m-d H:i:s'), 
                                    'Payments.due_time >' => date('Y-m-d H:i:s')
                                ]);
                    }
                ])
                ->first();

        if ($user->isfree == '0' && $user->type != 'press') {
            if (empty($user->payments)) {
                return $this->redirect(['controller'=>'Payments','action' => 'proceed']);
            }
        }
        //echo "Country: ".$country;
        $cntry = (isset($this->request->getQuery()['country_id']))? $this->request->getQuery()['country_id'] : "";
        $cat = (isset($this->request->getQuery()['category_id']))? $this->request->getQuery()['category_id'] : "";
        //$query = implode("-", $this->request->getQuery());
        $params =[]; 
        if (!empty($cntry)) $params['country_id']=$cntry;
        if (!empty($cat)) $params['category_id']=$cat;
        $params['visible'] = '1';

        $search_res = $this->paginate($this->Users->find()->where($params)->contain(['Categories'])->order(['registration_time'=>'DESC']));
        $this->set(compact('cat', 'cntry','search_res'));
    }

    public function changepassword(){
        if($this->request->is("post")){
            $id = $this->request->session()->read('Auth.User.id');
            $user = $this->Users->get($id); 
            $passwordhasher = new DefaultPasswordHasher;
            if (!$passwordhasher->check($this->request->getData()['oldpass'], $user->password)) {
                echo "oldpassnomatch";
            }else if (strlen($this->request->getData()['newpass']) < 6) {
                echo "lengtherror";
            }else if ($this->request->getData()['newpass'] != $this->request->getData()['confirmnewpass']) {
                echo "newpassnomatch";
            }else{
                $user = $this->Users->patchEntity($user,['password'=> $this->request->getData()['newpass']]);
                    if ($this->Users->save($user)) {
                        $this->logout();
                        echo "success";
                    }else{
                        echo "fail";
                    }
            }
            die();
        }
    }

    public function test(){
        $this->sendemail('yassine.elkhanboubi@gmail.com');
    }
}
