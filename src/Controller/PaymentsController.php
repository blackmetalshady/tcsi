<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\ExecutePayment;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use angelleye\PayPal\Adaptive;
use Cake\Network\Exception\NotFoundException;

/**
 * Payement Controller
 *
 *
 * @method \App\Model\Entity\Payement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PaymentsController extends AppController
{

    public function pay(){
    	//print_r($this->request); die();
        $usr = $this->request->session()->read('Auth.User');
        $payments = $this->Payments->find()
                       ->where([
                            'user_id'=>$usr['id'],
                            'create_time <' => Time::now(), 
                            'due_time >' => Time::now()])
                       ->first();
        if (!empty($payments)) return $this->redirect(['controller'=>'Users', 'action'=>'profile']);

        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                /*'Ac9EWrOP5-4VfIrVBYkWvWN6w76qIhyVT6ChQewTRGLcVBqI0dwjOidUj7c1CwbscEsvrrtl3ue_44k2', //SandBox ClientID
            	'EKk6zfAgTrewQHTVmuSKrm5BdQMJdUOQ9TC610OeWNg4XCPUWIyylklODIjcqc8SZb4psReR3xlzX1et'*/ //Sandbox ClientSecret
                'AXIyVX6MDRz6WX0ufZYEfLPXD0I0M5BjN_MWQO_ZMWaBFHhEmOBDESJ-wJWS35VL8kDzVyadsYXB9O6V', // Live ClientID
                'EBszbBm1ORpxe13BlBSzzG6SKOIeib5zR2042SjQ219u_fOpPPZmFkXrVDxHVBnxvR6g171yAw8HSJFR'  // Live ClientSecret
            )
        );

        $usr = $this->request->session()->read('Auth.User');
         $mnt = '0.00';
         if ($usr['type'] === 'particular') {
             $mnt = '120.00';         
         }elseif ($usr['type'] === 'company' || $usr['type'] === 'artist') {
             $mnt = '550.00';
         }
        // After Step 2
        $payer = new \PayPal\Api\Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new \PayPal\Api\Amount();
        $amount->setTotal($mnt);
        $amount->setCurrency('EUR');

        $transaction = new \PayPal\Api\Transaction();
        $transaction->setAmount($amount);

        $redirectUrls = new \PayPal\Api\RedirectUrls();
        $redirectUrls->setReturnUrl("https://www.theconceptstoreinternational.fr/payments/exec")
            ->setCancelUrl("https://www.theconceptstoreinternational.fr/payments/cancel");

        $payment = new \PayPal\Api\Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);

        // After Step 3
        try {
            $apiContext->setConfig(
                array(
                    'log.LogEnabled' => true,
                    'log.FileName' => 'PayPal.log',
                    'log.LogLevel' => 'FINE',
                    'mode' => 'live',
                )
            );
            $payment->create($apiContext);
            //echo $payment;

            //echo "\n\nRedirect user to approval_url: " . $payment->getApprovalLink() . "\n";
            header('Location: '.$payment->getApprovalLink());
            die();
        }
        catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo "Erreur de Connexion avec le service PayPal, Veuillez ressayer plutard";
            // This will print the detailed information on the exception.
            //REALLY HELPFUL FOR DEBUGGING
            echo $ex->getData();
            die();
        }
    }

    public function exec(){
        if ($this->request->is('get')) {
            $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    /*'Ac9EWrOP5-4VfIrVBYkWvWN6w76qIhyVT6ChQewTRGLcVBqI0dwjOidUj7c1CwbscEsvrrtl3ue_44k2', //SandBox ClientID
                	'EKk6zfAgTrewQHTVmuSKrm5BdQMJdUOQ9TC610OeWNg4XCPUWIyylklODIjcqc8SZb4psReR3xlzX1et'*/ //Sandbox ClientSecret
                    'AXIyVX6MDRz6WX0ufZYEfLPXD0I0M5BjN_MWQO_ZMWaBFHhEmOBDESJ-wJWS35VL8kDzVyadsYXB9O6V', // Live ClientID
                	'EBszbBm1ORpxe13BlBSzzG6SKOIeib5zR2042SjQ219u_fOpPPZmFkXrVDxHVBnxvR6g171yAw8HSJFR'  // Live ClientSecret
                )
            );
            $apiContext->setConfig(
                array(
                    'log.LogEnabled' => true,
                    'log.FileName' => 'PayPal.log',
                    'log.LogLevel' => 'FINE',
                    'mode' => 'live',
                )
            );
            $data = $this->request->getQueryParams();
            // Get payment object by passing paymentId
            $paymentId = $data['paymentId'];
            $payment = Payment::get($paymentId, $apiContext);
            $payerId = $data['PayerID'];

            // Execute payment with payer ID
            $execution = new PaymentExecution();
            $execution->setPayerId($payerId);

            try {
              // Execute payment
              $result = $payment->execute($execution, $apiContext);
              if ($result->getState() === 'approved') {
                //print_r($result); die();
                    $t_data = [
                        'id' => $result->getId(),
                        'intent' => $result->getIntent(),
                        'status' => $result->getState(),
                        'total' => $result->getTransactions()[0]->getAmount()->total,
                        'currency' => $result->getTransactions()[0]->getAmount()->currency,
                        'transaction_id' => $result->getTransactions()[0]->related_resources[0]->sale->id,
                        'payer' => $result->getPayer()->getPayerInfo()->getPayerId(),
                        'firstname' => $result->getPayer()->getPayerInfo()->getFirstName(),
                        'lastname' => $result->getPayer()->getPayerInfo()->getLastName(),
                        'email' => $result->getPayer()->getPayerInfo()->getEmail(),
                        'address' => $result->getPayer()->getPayerInfo()->shipping_address->line1,
                        'recipient_name' => $result->getPayer()->getPayerInfo()->shipping_address->recipient_name,
                        'city' => $result->getPayer()->getPayerInfo()->shipping_address->city,
                        'state' => $result->getPayer()->getPayerInfo()->shipping_address->state,
                        'postal_code' => $result->getPayer()->getPayerInfo()->shipping_address->postal_code,
                        'country_code' => $result->getPayer()->getPayerInfo()->shipping_address->country_code,
                        'create_time' => date('Y-m-d H:i:s'),
                        'due_time' => date('Y-m-d H:i:s', strtotime('+1 year')),
                        'user_id' => $this->request->session()->read('Auth.User.id')
                    ];
                    $usr_payment = $this->Payments->newEntity();
                    $usr_payment = $this->Payments->patchEntity($usr_payment, $t_data);
                    //print_r($usr_payment);die();
                    if ($pay = $this->Payments->save($usr_payment)) {
                        //$this->createSendInvoice();
                        //$html = file_get_contents(filename)
                        return $this->redirect(['controller'=>'Payments','action' => 'success']);
                    }else{
                        print_r($usr_payment);
                    }
              }
              die();
            } catch (\Exception $ex) {
            	return $this->redirect([
	            			'controller'=>'Payments',
	            			'action' => 'proceed',
	            			'?' => ['result' => 'fail']
						]);

              //echo $ex->getCode();
              //echo $ex->getData();
              die($ex);
            } catch (Exception $ex) {
              die($ex);
            }
        }
    }

    public function invoice($id){
            $invoice = $this->Payments->find()->where(['Payments.id'=>$id])->first();        

        if (empty($invoice)) {
            throw new NotFoundException(__('Invoice Not Found'));
        }
        $this->viewBuilder()->layout(false);
        $this->set(compact('invoice'));
    }

    public function proceed(){
    	$params = $this->request->getQueryParams();
    	//print_r($params); die();
    	$msg = "Vous devez Payer pour Continuer";
    	if (!empty($params['result']) && $params['result'] === "fail") {
    		$msg = '<i class="fas fa-exclamation-circle" style="font-size: 25px;"></i> <span style="vertical-align: text-bottom;">Paiement rejeté ou carte invalide, veuillez vérifier vos paramètres de paiement</span>';
    	}

        $usr = $this->request->session()->read('Auth.User');
        $payments = $this->Payments->find()
                       ->where([
                            'user_id'=>$usr['id'],
                            'create_time <' => Time::now(), 
                            'due_time >' => Time::now()])
                       ->first();
        if (!empty($payments)) return $this->redirect(['controller'=>'Users', 'action'=>'profile']);
        //print_r($payments); die();
        $mnt = '0.00';
        if ($usr['type'] === 'artist') {
            $amount = '120.00';         
        }elseif ($usr['type'] === 'company') {
            $amount = '550.00';
        }

        $start_date = date('Y-m-d');
        $due_date = date('Y-m-d', strtotime('+1 year'));

        $this->set(compact(['amount','start_date', 'due_date', 'msg']));
    }
    
    public function success(){
        $usr = $this->request->session()->read('Auth.User');
        $payments = $this->Payments->find()
                       ->where([
                            'user_id'=>$usr['id'],
                            'create_time <' => Time::now(), 
                            'due_time >' => Time::now()])
                       ->first();
        if (!empty($payments)) return $this->redirect(['controller'=>'Users', 'action'=>'profile']);
    }

    private function createSendInvoice(){
        // Include required library files.
        $usr = $this->request->session()->read('Auth.User');
         $amount = '0.00';
         if ($usr['type'] === 'artist') {
             $amount = '120.00';         
         }elseif ($usr['type'] === 'company') {
             $amount = '550.00';
         }
        // Create PayPal object.
        $PayPalConfig = array(
            'Sandbox' => false,
            'DeveloperAccountEmail' => 'contact-facilitator@comingoutstudio.com',
            'ApplicationID' => 'APP-80W284485P519543T',
            'DeviceID' => '',
            'IPAddress' => $_SERVER['REMOTE_ADDR'],
            'APIUsername' => 'contact-facilitator_api1.comingoutstudio.com',
            'APIPassword' => 'DTE8BJTHZN23A4LH',
            'APISignature' => 'AEZfwUtR1I5Ak8hWrvsi84MDT8pfAXI305gxC7SrdzQislzqKL9ol9Xy',
            'APISubject' => ''
        );
         
        $PayPal = new Adaptive($PayPalConfig);
        $now = Time::now();
        $now->i18nFormat('yyyy-MM-dd');
        $date = $now->year.'-'.$now->month.'-'.$now->day;
        // Prepare request arrays
        $CreateInvoiceFields = array(
            'MerchantEmail' => 'contact-facilitator@comingoutstudio.com', // Required. Merchant email address.
            'PayerEmail' => 'yassine.elkhanboubi@gmail.com', // Required. Payer email address.
            'Number' => 'TCSI-'.time(), // Unique ID for the invoice.
            'CurrencyCode' => 'EUR', // Required. Currency used for all invoice item amounts and totals.
            'InvoiceDate' => '', // Date on which the invoice is enabled.
            'DueDate' => '', // Date on which the invoice payment is due.
            'PaymentTerms' => 'DueOnReceipt', // Required. Terms by which the invoice payment is due. Values are: DueOnReceipt, DueOnSpecified, Net10, Net15, Net30, Net45
            //'DiscountPercent' => '', // Discount percent applied to the invoice.
            //'DiscountAmount' => '', // Discount amount applied to the invoice. If DiscountPercent is provided, DiscountAmount is ignored.
            'Terms' => '', // General terms for the invoice.
            'Note' => '', // Note to the payer company.
            'MerchantMemo' => '', // Memo for bookkeeping that is private to the merchant.
            //'ShippingAmount' => '0.00', // Cost of shipping
            //'ShippingTaxName' => '', // Name of the applicable tax on the shipping cost.
            //'ShippingTaxRate' => '', // Rate of the applicable tax on the shipping cost.
            'LogoURL' => 'https://www.theconceptstoreinternational.fr/img/logo.jpg'    // Complete URL to an external image used as the logo, if any.
        );
         
        $BusinessInfo = array(
            'FirstName' => 'Olivier', // First name of the company contact.
            'LastName' => 'Delasalle', // Last name of the company contact.
            'BusinessName' => 'TheConceptStoreInternational', // Company business name.
            'Phone' => '+212677354799', // Phone number for contacting the company.
            'Fax' => '', // Fax number used by the company.
            'Website' => 'https://www.theconceptstoreinternational.fr', // Website used by the company.
            'Custom' => 'Some custom info.' // Custom value to be displayed in the contact information details.
        );
         
        $BusinessInfoAddress = array(
            'Line1' => 'Guiliz Marrakech', // Required. First line of address.
            'Line2' => '', // Second line of address.
            'City' => 'Marrakech', // Required. City of thte address.
            'State' => 'MA', // State for the address.
            'PostalCode' => '40000', // Postal code of the address
            'CountryCode' => 'MA'    // Required. Country code of the address.
        );
         
        /*$BillingInfo = array(
            'FirstName' => 'Tester', // First name of the company contact.
            'LastName' => 'Testerson', // Last name of the company contact.
            'BusinessName' => 'Testers, LLC', // Company business name.
            'Phone' => '555-555-5555', // Phone number for contacting the company.
            'Fax' => '555-555-5556', // Fax number used by the company.
            'Website' => 'http://www.domain.com', // Website used by the company.
            'Custom' => 'Some custom info.' // Custom value to be displayed in the contact information details.
        );
         
        $BillingInfoAddress = array(
            'Line1' => '123 Main St.', // Required. First line of address.
            'Line2' => '', // Second line of address.
            'City' => 'Grandview', // Required. City of thte address.
            'State' => 'MO', // State for the address.
            'PostalCode' => '64030', // Postal code of the address
            'CountryCode' => 'US'    // Required. Country code of the address.
        );
         
        $ShippingInfo = array(
            'FirstName' => 'Tester', // First name of the company contact.
            'LastName' => 'Testerson', // Last name of the company contact.
            'BusinessName' => 'Testers, LLC', // Company business name.
            'Phone' => '555-555-5555', // Phone number for contacting the company.
            'Fax' => '555-555-5556', // Fax number used by the company.
            'Website' => 'http://www.domain.com', // Website used by the company.
            'Custom' => 'Some custom info.' // Custom value to be displayed in the contact information details.
        );
         
        $ShippingInfoAddress = array(
            'Line1' => '123 Main St.', // Required. First line of address.
            'Line2' => '', // Second line of address.
            'City' => 'Grandview', // Required. City of thte address.
            'State' => 'MO', // State for the address.
            'PostalCode' => '64030', // Postal code of the address
            'CountryCode' => 'US'    // Required. Country code of the address.
        );*/
         
        // For invoice items you populate a nested array with multiple $InvoiceItem arrays. Normally you'll be looping through cart items to populate the $InvoiceItem
        // array and then push it into the $InvoiceItems array at the end of each loop for an entire collection of all items in $InvoiceItems.
         
        $InvoiceItems = array();

        $InvoiceItem = array(
            'Name' => 'Test Widget 1', // Required. SKU or name of the item.
            'Description' => 'This is a test widget #1', // Item description.
            'Date' => '', // Date on which the product or service was provided.
            'Quantity' => '1', // Required. Item count. Values are: 0 to 10000
            'UnitPrice' => $amount, // Required. Price of the item, in the currency specified by the invoice.
            'TaxName' => '', // Name of the applicable tax.
            'TaxRate' => ''  // Rate of the applicable tax.
        );
        array_push($InvoiceItems,$InvoiceItem);
         
        $PayPalRequestData = array(
            'CreateInvoiceFields' => $CreateInvoiceFields,
            'BusinessInfo' => $BusinessInfo,
            'BusinessInfoAddress' => $BusinessInfoAddress,
            /*'BillingInfo' => $BillingInfo,
            'BillingInfoAddress' => $BillingInfoAddress,
            'ShippingInfo' => $ShippingInfo,
            'ShippingInfoAddress' => $ShippingInfoAddress,*/
            'InvoiceItems' => $InvoiceItems
        );
         
        // Pass data into class for processing with PayPal and load the response array into $PayPalResult
        $PayPalResult = $PayPal->CreateAndSendInvoice($PayPalRequestData);
         
        // Write the contents of the response array to the screen for demo purposes.
        echo '<pre />';
        print_r($PayPalResult);
    }

    public function cancel()
    {
    	echo "Payment canceled by user  <a href='/users/profile'>return to your profile</a> ";
    	die();
    }
}
