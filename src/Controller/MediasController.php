<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Medias Controller
 *
 *
 * @method \App\Model\Entity\Media[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MediasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $medias = $this->paginate($this->Medias);

        $this->set(compact('medias'));
    }

    /**
     * View method
     *
     * @param string|null $id Media id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $media = $this->Medias->get($id, [
            'contain' => []
        ]);

        $this->set('media', $media);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $media = $this->Medias->newEntity();
        if ($this->request->is('post')) {
            $media = $this->Medias->patchEntity($media, $this->request->getData());
            if ($this->Medias->save($media)) {
                $this->Flash->success(__('The media has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The media could not be saved. Please, try again.'));
        }
        $this->set(compact('media'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Media id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $media = $this->Medias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $media = $this->Medias->patchEntity($media, $this->request->getData());
            if ($this->Medias->save($media)) {
                $this->Flash->success(__('The media has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The media could not be saved. Please, try again.'));
        }
        $this->set(compact('media'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Media id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        if ($this->request->is(['post','delete']) && !empty($this->request->getData()['filename'])) {
            //print_r($this->request->getData()); die();
            $this->loadModel('Pictures');
            $media = $this->Pictures->find()->where(['filename'=>$this->request->getData()['filename']])->first();
            if (empty($media)) {echo "fail"; die();}
            if ($this->Pictures->delete($media)) {
                $filepath = WWW_ROOT.'img/gallerie/'.$this->request->session()->read('Auth.User.id').'/'.$this->request->getData()['filename'];
                unlink($filepath);
                echo json_encode(['status'=>'success','filename'=>$media->filename,'pid'=>$media->id]);
                die();
            } else {
                echo "fail";die();
            }
        }
        echo "fail";die();
    }

        public function upload(){
            if ($this->request->is('post')) {
                $upload_dir = WWW_ROOT.'img/gallerie/'.$this->request->session()->read('Auth.User.id');
                $filename   = basename($this->request->data['img']['name']);
                $extension  = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
                $newname    = $this->request->session()->read('Auth.User.id').'_'.time().'.'.$extension;
                $finalfile  = $upload_dir.'/'.$newname;
            
                if(move_uploaded_file($this->request->data['img']['tmp_name'], $finalfile)){
                    $this->loadModel("Pictures");
                    $media = $this->Pictures->newEntity();
                    $newEntity = ['user_id'=>$this->request->session()->read('Auth.User.id'), 'filename'=>$newname];
                    $media = $this->Pictures->patchEntity($media, $newEntity);
                    
                    if ($result = $this->Pictures->save($media)) {
                        echo $result->filename;
                    }
                    
                }else{
                    echo "fail";
                }
            }
            die();
    }

    public function uploadGallery(){
        if ($this->request->is('post')) {
            
            $usr = (object) $this->request->session()->read('Auth.User');
            $data['uid'] = $usr->id;
            //print_r($this->request->getData()); die();
            $this->loadModel("Pictures");
            if (!file_exists(WWW_ROOT.'img/gallerie/'.$usr->id) || !is_dir(WWW_ROOT.'img/gallerie/'.$usr->id)) {
                mkdir(WWW_ROOT.'img/gallerie/'.$usr->id, 0777);
            }
            foreach ($this->request->getData() as $pic) {
                //print_r($pic); die();
                if (!empty($pic)) {
                    $ext = strtolower(pathinfo($pic['name'], PATHINFO_EXTENSION));
                    $filename = md5($usr->id.time()).'.'.$ext;
                    $finalname = WWW_ROOT.'img/gallerie/'.$usr->id.'/'.$filename;
                    if(move_uploaded_file($pic['tmp_name'], $finalname)){
                        chmod($finalname, 0766);
                        $photo = $this->Pictures->newEntity();
                        $photo = $this->Pictures->patchEntity($photo, ['user_id'=>$usr->id, 'filename'=>$filename]) ;
                        if ($pic = $this->Pictures->save($photo)) {
                            $data['file']=$filename;
                            $data['pid']=$pic->id;
                        }else{
                            print_r($photo); die();
                        }
                        
                        //array_push($pictures, $usr->id."/".$filename);
                    }else{
                        print_r($photo); die();
                    }
                }
            }
            echo json_encode($data);die();
        }
    }
}
