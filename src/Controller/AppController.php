<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'primaryemail']
                ]
            ],
            'logoutRedirect' => '/',
            'loginRedirect' => '/',
        ]);

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
        $this->loadModel('Countries');
        $countries = $this->Countries->find()->order(['name'=>'ASC']);

        $this->loadModel('Categories');
        $categories = $this->Categories->find()->where(['ispress'=>'0'])->order(['position'=>'ASC']);
        $categoriespress = $this->Categories->find()->where(['ispress'=>'1'])->order(['position'=>'ASC']);
        $authenticated = $this->Auth->user();
        $this->set(compact(['authenticated','countries','categories','categoriespress']));
    }
        public function sendemail($mail){

        //print_r($mail); die();
            $message = '<center><img src="http://'.$this->request->env('HTTP_HOST').'/img/logo.jpg" style="width: 150px"></center><br><br><span style="font-size: 18px; margin-bottom: 20px;"> TheConceptStoreInternational vous remercie de votre inscription, vous pouvez commancer à naviguer sur le site <a href="https://www.theconceptstoreinternational.fr/users/login"> on se connectant</a></span><br><br><br><br><br><span style="font-size: 16px;">Cordialement</span><br><span style="font-size: 16px;">Equipe TheConceptStoreInternational</span>';
        //echo $message; die();
        $email = new Email('default');
        $email->from(['admin@theconceptstoreinternational.com' => 'TheConceptStoreInternational'])
            ->emailFormat('html')
            ->to($mail)
            ->subject('Bienvenue sur TheConceptStoreInternational');

            if(!$email->send($message)) {
                CakeLog::write('debug', $email->smtpError);
            }
            //$email->send($message);
            try {
                $email->send($message);
            } catch (\Exception $e) {
                return false;
            }
            return true;
    }

    public function sendadminemail($details, $uid){

        if (!empty($details['profilepic'])) {
            $img_file_name = $this->base64toimg($details, $uid);
            $imageSrc = "users/".$img_file_name;
        }else{
            $imageSrc = "logo.jpg";
        }
        unset($details['type']);
        unset($details['profilepic']);
        unset($details['gallerie']);
        $infodetails="<ul style='font-size: 18px;'>";
            foreach ($details as $k => $v) {
                if(!empty($v)){
                    $infodetails.="<li><u><b>".$k."</b></u> : ".$v."</li>";
                }
                
            }
            $infodetails.="</ul>";
            $message = '<center><img src="http://'.$this->request->env('HTTP_HOST').'/img/'.$imageSrc.'" style="width: 150px"></center><br><br><span style="font-size: 18px; margin-bottom: 20px;"> Nouvelle inscription sur TheConceptStoreInternational.com </span><br><br><br><span>Details</span><br><br><div>'.$infodetails.'</div>';
        //echo $message; die();
        $email = new Email('default');
        $email->from(['admin@theconceptstoreinternational.com' => 'TheConceptStoreInternational'])
            ->emailFormat('html')
            ->to('theconceptstoreinternation@gmail.com')
            ->subject('Nouvelle Inscription sur TheConceptStoreInternational');
        
        try {
            $email->send($message);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public function sendinvoiceemail($invoice_id){

            $message = '<center><img src="http://'.$this->request->env('HTTP_HOST').'/img/'.$imageSrc.'" style="width: 150px"></center><br><br><span style="font-size: 18px; margin-bottom: 20px;"> TheConceptStoreInternational.com vous remercie de votre paiement, vous pouvez consulter votre facture <a href="https://www.theconceptstoreinternational.fr/payments/invoice/'.$invoice_id.'">ici</a></span><br><br><br><span>Détails</span><br><br><div>'.'MON DETAIL'.'</div>';
        //echo $message; die();
        $email = new Email('default');
        $email->from(['admin@theconceptstoreinternational.com' => 'TheConceptStoreInternational'])
            ->emailFormat('html')
            ->to($this->Auth->user()['primaryemail'])
            ->subject('Nouvelle Inscription sur TheConceptStoreInternational');
        
        try {
            $email->send($message);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public function sendcontactemail($data){
        
        $infodetails="<ul style='font-size: 18px;'>";
            foreach ($details as $k => $v) {
                if(!empty($v)){
                    $infodetails.="<li><u><b>".$k."</b></u> : ".$v."</li>";
                }
                
            }
            $infodetails.="</ul>";
            $message = '<br/><br/><div>vous avez un nouveau message de la part de : <b>'.$data['email'].'</b></div><br/><p><b>Sujet : </b> '.$data['subject'].'</p><p><div><b><u>Message</u></b></div><br/>'.$data['message'].'</p>';
        //echo $message; die();
        $email = new Email('default');
        $email->from(['admin@theconceptstoreinternational.com' => 'TheConceptStoreInternational'])
            ->emailFormat('html')
            ->to('theconceptstoreinternation@gmail.com')
            ->subject('Nouveau Message sur TheConceptStoreInternational');
        
        try {
            $email->send($message);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public function base64toimg($data, $uid){
        //$exp = explode(',', $data['profilepic']);
        $image_parts = explode(";base64,", $data['profilepic']);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $basedecoded = base64_decode($image_parts[1]);
        $file = WWW_ROOT.'img/users/'.$uid.'.'.$image_type;
        file_put_contents($file, $basedecoded);
        return $uid.'.'.$image_type;
        //print $success ? $file : 'Unable to save the file.'; die();
    }
}
