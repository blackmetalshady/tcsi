<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Payments Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Payment get($primaryKey, $options = [])
 * @method \App\Model\Entity\Payment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Payment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Payment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Payment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Payment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Payment findOrCreate($search, callable $callback = null, $options = [])
 */
class PaymentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('id')
            ->maxLength('status', 255)
            ->requirePresence('id', 'create')
            ->notEmpty('id');

        $validator
            ->maxLength('payer', 255)
            ->requirePresence('payer', 'create')
            ->notEmpty('payer');

        $validator
            ->maxLength('transaction_id', 255)
            ->requirePresence('transaction_id', 'create')
            ->notEmpty('transaction_id');

        $validator
            ->maxLength('firstname', 255)
            ->requirePresence('firstname', 'create')
            ->allowEmpty('firstname');

        $validator
            ->maxLength('lastname', 255)
            ->requirePresence('lastname', 'create')
            ->allowEmpty('lastname');

        $validator
            ->maxLength('email', 255)
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->maxLength('address', 255)
            ->requirePresence('address', 'create')
            ->allowEmpty('address');

        $validator
            ->maxLength('recipient_name', 255)
            ->requirePresence('recipient_name', 'create')
            ->allowEmpty('recipient_name');

        $validator
            ->maxLength('city', 255)
            ->requirePresence('city', 'create')
            ->allowEmpty('city');

        $validator
            ->notEmpty('state')
            ->maxLength('state', 255)
            ->requirePresence('state', 'create')
            ->allowEmpty('state');

        $validator
            ->notEmpty('postal_code')
            ->maxLength('postal_code', 255)
            ->requirePresence('postal_code', 'create')
            ->allowEmpty('postal_code');

        $validator
            ->notEmpty('country_code')
            ->maxLength('country_code', 255)
            ->requirePresence('country_code', 'create')
            ->allowEmpty('country_code');

        $validator
            ->scalar('intent')
            ->maxLength('intent', 255)
            ->requirePresence('intent', 'create')
            ->notEmpty('intent');

        $validator
            ->scalar('status')
            ->maxLength('status', 255)
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->numeric('total')
            ->requirePresence('total', 'create')
            ->notEmpty('total');

        $validator
            ->scalar('currency')
            ->maxLength('currency', 4)
            ->requirePresence('currency', 'create')
            ->notEmpty('currency');

        $validator
            ->dateTime('create_time')
            ->requirePresence('create_time', 'create')
            ->notEmpty('create_time');

        $validator
            ->dateTime('due_time')
            ->requirePresence('due_time', 'create')
            ->notEmpty('due_time');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
