<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property string $id
 * @property string $type
 * @property string $firstname
 * @property string $lastname
 * @property string $company
 * @property string $category_id
 * @property string $presentation
 * @property string $country
 * @property string $city
 * @property string $address
 * @property int $zipcode
 * @property string $phone
 * @property string $fax
 * @property string $gsm
 * @property string $primaryemail
 * @property string $password
 * @property string $secondaryemail
 * @property string $website
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $pinterest
 * @property string $profilepic
 * @property int $isfree
 * @property \Cake\I18n\FrozenTime $registration_time
 *
 * @property \App\Model\Entity\Picture[] $pictures
 * @property \App\Model\Entity\Category $category
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type' => true,
        'firstname' => true,
        'lastname' => true,
        'company' => true,
        'category_id' => true,
        'presentation' => true,
        'country_id' => true,
        'city' => true,
        'address' => true,
        'zipcode' => true,
        'phone' => true,
        'fax' => true,
        'gsm' => true,
        'primaryemail' => true,
        'password' => true,
        'secondaryemail' => true,
        'website' => true,
        'facebook' => true,
        'twitter' => true,
        'instagram' => true,
        'pinterest' => true,
        'profilepic' => true,
        'registration_time' => true,
        'pictures' => true,
        'category' => true,
        'isfree' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }
}
