<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Payment Entity
 *
 * @property string $id
 * @property string $payer
 * @property string $intent
 * @property string $status
 * @property float $total
 * @property string $currency
 * @property \Cake\I18n\FrozenTime $create_time
 * @property \Cake\I18n\FrozenTime $due_time
 * @property string $user_id
 *
 * @property \App\Model\Entity\User $user
 */
class Payment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'payer' => true,
        'transaction_id' => true,
        'address' => true,
        'recipient_name' => true,
        'line1' => true,
        'city' => true,
        'state' => true,
        'postal_code' => true,
        'country_code' => true,
        'firstname' => true,
        'lastname' => true,
        'email' => true,
        'intent' => true,
        'status' => true,
        'total' => true,
        'currency' => true,
        'create_time' => true,
        'due_time' => true,
        'user_id' => true,
        'user' => true
    ];
}
