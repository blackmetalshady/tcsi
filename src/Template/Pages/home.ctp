<?php $this->assign('title','The Concept Store International'); ?>
<div class="row">
    <div class="col-md-9" style="margin-left: auto;margin-right: auto; color: #fff;font-size: 20px; margin-bottom: 20px;">
        <marquee>
            <div style="display: inline-block;margin-right:50px; ">Afin d'avoir accès aux informations figurant sur ce site. Inscrivez-vous</div>
            <div style="display: inline-block;margin-right:50px; ">-</div>
            <div style="display: inline-block;margin-right:50px; ">In order to have access to the information on this site. Sign up</div>
            <div style="display: inline-block;margin-right:50px; ">-</div>
            <div style="display: inline-block;margin-right:50px; ">Para tener acceso a la información de este sitio. contratar</div>
        </marquee>
    </div>
</div>
<div class="row" style="background-color: #000;">
    <div class="col-md-9" style="margin-left: auto;margin-right: auto;">
        <div class="owl-carousel owl-theme owl-loaded home-carousel">
                <div style="position: relative;"><?= $this->Html->image('slider/1.jpg'); ?>
                    <div class="slidetext"><p>Stylistes Hommes</p></div>
                </div>
                <div style="position: relative;"><?= $this->Html->image('slider/2.jpg'); ?>
                    <div class="slidetext"><p>Stylistes Femmes</p></div>
                </div>
                <div style="position: relative;"><?= $this->Html->image('slider/3.jpg'); ?>
                    <div class="slidetext"><p>Stylistes Enfants</p></div>
                </div>
                <div style="position: relative;"><?= $this->Html->image('slider/4.jpg'); ?>
                    <div class="slidetext"><p>Créateurs d’accessoires modes, bijoux fantaisie, joaillerie</p></div>
                </div>
                <div style="position: relative;"><?= $this->Html->image('slider/5.jpg'); ?>
                    <div class="slidetext"><p>Cosmétiques, de parfums, produits bio</p></div>
                </div>
                <div style="position: relative;"><?= $this->Html->image('slider/6.jpg'); ?>
                    <div class="slidetext"><p>Artistes peintres, Photographes, Sculpteurs</p></div>
                </div>
                <div style="position: relative;"><?= $this->Html->image('slider/7.jpg'); ?>
                    <div class="slidetext"><p>Designers meubles, d’objets, luminaires</p></div>
                </div>
                <div style="position: relative;"><?= $this->Html->image('slider/8.jpg'); ?>
                    <div class="slidetext"><p>Galeries d’arts</p></div>
                </div>
                <div style="position: relative;"><?= $this->Html->image('slider/9.jpg'); ?>
                    <div class="slidetext"><p>Concepts Stores</p></div>
                </div>
                <div style="position: relative;"><?= $this->Html->image('slider/10.jpg'); ?>
                    <div class="slidetext"><p>Presse Ecrite & Presse en ligne</p></div>
                </div>
        </div>
    </div>    
</div>
<style type="text/css">
    #marqee {
      width: 200px; height: 50px; white-space: nowrap;
      overflow: hidden;
      overflow-x:-webkit-marquee;
      -webkit-marquee-direction: forwards;
      -webkit-marquee-style: scroll;
      -webkit-marquee-speed: normal;
      -webkit-marquee-increment: small;
      -webkit-marquee-repetition: 5;
      overflow-x: marquee-line;
      marquee-direction: forward;
      marquee-style: scroll;
      marquee-speed: normal;
      marquee-play-count: 5;
    }
</style>