<?php $this->assign('title','Nous Contacter - The Concept Store International'); ?>
<div class="row" style="border-color: #fff; margin-left: auto;margin-right: auto;color: #fff; width: 80%;margin-top: 50px;margin-bottom: 50px;">
	<div class="col-sm" style="width: 90%;border-left: 1px solid #fff;display: table; height: 100px;">
		<div style="vertical-align: middle;display: table-cell;">
			<!-- <i class="fa fa-phone"  style="font-size: 35px;"></i>
			<div>0677354799</div>
			<div>0665945653</div> -->
			<div>Mr Faiçal BNOUAADI</div>
			<div>Directeur de la publication</div>
			<div>Gsm : 00.212.6.65.94.56.53</div>
			<div>
				<a href="mailto:direction@theconceptstoreinternational.com">direction@theconceptstoreinternational.com </a>
			</div>
		</div>
	</div>
	<div class="col-sm" style="width: 90%; border-right: 1px solid #fff;border-left: 1px solid #fff;display: table; height: 100px;">
		<div style="vertical-align: middle;display: table-cell;">
			<!--<i class="fa fa-envelope" style="font-size: 35px;"></i>
			<div>contact@theconceptstoreinternational.com</div>-->
			<div>Mr Olivier DELESALLE</div>
			<div>Directeur Marketing&Commercial</div>
			<div>Tél : 00.212.6.77.35.47.99</div>
			<div>
				<a href="mailto:marketing@theconceptstoreinternational.com">marketing@theconceptstoreinternational.com</a>
			</div>
			<div>
				<a href="mailto:commercial@theconceptstoreinternational.com">commercial@theconceptstoreinternational.com</a>
			</div>
		</div>
	</div>
	<div class="col-sm" style="width: 90%; border-right: 1px solid #fff;display: table; height: 100px;">
		<div style="vertical-align: middle;display: table-cell;">
			<!--<i class="fa fa-envelope" style="font-size: 35px;"></i>
			<div>contact@theconceptstoreinternational.com</div>-->
			<div>Yassine El Khanboubi</div>
			<div>Webmaster</div>
			<div></div>
			<div>
				<a href="mailto:support@theconceptstoreinternational.com">support@theconceptstoreinternational.com</a>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-7" style="margin-left: auto;margin-right: auto;">
		<div style="text-align: center;"><?= $this->Html->image('loading.gif',['id'=>'loader','style'=>'width: 35px; margin-left: auto; margin-right: auto;display: none;']);?></div>
		<form id="contact-form">
			<div class="form-element">
				<input type="email" name="email" style="height: 38px;" placeholder="E-mail *" required>
			</div>
			<div class="form-element">
				<input type="text" name="subject" style="height: 38px;" placeholder="Sujet *" required>
			</div>
			<div class="form-element">
				<textarea cols="10" style="width: 90%;height: 120px;" name="message" placeholder="Votre message *" required></textarea>
			</div>
			<div class="form-element">
				<button type="submit" class="submit-btn" style="width: 90%;margin-left: 0;">Envoyer</button>
			</div>
		</form>
	</div>
</div>
