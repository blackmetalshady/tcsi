<?php $this->assign('title','Qui Somme Nous - The Concept Store International'); ?>
<div class="main">
    <p class="txt">
        Vous êtes créateur de mode, de bijoux, joaillier, d’accessoires, parfums, cosmétiques, artiste peintre, photographe, sculpteur, designer de meubles ou d’objets de décoration, produits naturels et bio, un concept store ou une galerie, inscription gratuite pour la presse écrite, presse en ligne, radio et télévision <br/>
        <a href="https://www.theconceptstoreinternational.com" target="_blank">www.theconceptstoreinternational.com</a>  vous propose plus qu’un annuaire, un site inédit, qui vous permet de rentrer directement en contact avec tous les concepts store & les galeries à travers le monde présent dans 197 pays. Lors de votre inscription avec toutes vos informations et vos photos, ainsi que les mises à jour de votre page, celle-ci sera envoyé systématiquement à toutes les personnes inscrites sur notre site. Tarifs d’inscription: 550 € à l’année. De plus notre société reversera la somme de 10 € par inscription à l’association les Enfants Dar Bouidar, en accord avec le fondateur et président Mr Hubert Hansjorg. <a href="https://atlas-children.org/fr/" target="_blank">https://atlas-children.org/fr/</a>
    </p>
    <p class="txt">
        Artist, painter, photographer, sculptor, or designer of furniture or decorative items, natural and organic products, a concept store or a gallery   free registration for the press, press online, radio and television<br/>

        <a href="https://www.theconceptstoreinternational.com" target="_blank">www.theconceptstoreinternational.com</a> proposes more than a directory, an unprecedented website which will allow You are a creator of fashion, accessories, jewelry, a jeweler, perfumes, cosmetics, you to connect directly with all concept stores and galleries throughout the world in 197 countries.<br/> Once you have enrolled including your details and photos and your page is updated, it will then be systematically sent to all of the people signed up on our website.  Registration fees: 550 € per year. <br/>
        In addition, our company will donate € 10 per registration to the Children Dar Bouidar association, in agreement with the founder and president Mr Hubert Hansjorg. <a href="https://atlas-children.org/fr/" target="_blank">https://atlas-children.org/fr/</a>
    </p>
    <p class="txt">
        Si Vd. es diseñador de moda, diseñador de joyas, accesorios, perfumes, cosméticos, pintor, fotógrafo, escultor, diseñador de muebles u objetos decorativos, productos naturales y orgánicos, una tienda de concepto o una galería,  prensa escrita, o prensa digital inscripción gratuita para la prensa, presione en línea, radio y television <br/>
        <a href="https://www.theconceptstoreinternational.com" target="_blank">www.theconceptstoreinternational.com</a> le ofrece más que un carné de direcciones, un sitio inédito, que le permite entrar directamente en contacto con todas las tiendas “concept store” y galerías de todo el mundo en 197 países. Al registrarse con todas sus informaciones y fotos, así como las actualizaciones de su página se enviarán sistemáticamente a todas las personas registradas en nuestro sitio web. Gastos de inscripción: 550 € por año. <br/>
        Además, nuestra empresa donará 10 € por inscripción a la asociación Children Dar Bouidar, de acuerdo con el fundador y presidente, el Sr. Hubert Hansjorg. <a href="https://atlas-children.org/fr/" target="_blank">https://atlas-children.org/fr/</a>
    </p>
</div>
<br/>
