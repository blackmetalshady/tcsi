<?php $this->assign('title','TCSI - Communiqués de Presse'); ?>
<div class="col-md-9 profile-container">
	<h1 style="color: #fff">Communiqués de Presse</h1>
	<div class="grid-container" style="width: 85%;">
		<div class="grid-item">
			<div class="cpress">
				<div class="lang">
					<?= $this->Html->link($this->Html->image('flags/fr.png'), '/files/Communiqué_de_Presse.pdf',['target'=>'_blank','escape'=>false]); ?>
				</div>
				<?= $this->Html->image('cp-fr.png',['class'=>'cp-img']); ?>
			</div>
		</div>
		<div class="grid-item">
			<div class="cpress">
				<div class="lang">
					<?= $this->Html->link($this->Html->image('flags/en.png'), '/files/Press_Release.pdf',['target'=>'_blank','escape'=>false]); ?>
				</div>
				<?= $this->Html->image('cp-en.png',['class'=>'cp-img']); ?>
			</div>
		</div>
		<div class="grid-item">
			<div class="cpress">
				<div class="lang">
					<?= $this->Html->link($this->Html->image('flags/es.jpg'), '/files/Comuicade_de_Prensa.pdf',['target'=>'_blank','escape'=>false]); ?>
				</div>
				<?= $this->Html->image('cp-es.png',['class'=>'cp-img']); ?>
			</div>
		</div>
	</div>
</div>