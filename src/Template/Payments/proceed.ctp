<div class="col-md-9" id="payment-container">
	<a href="/Payments/proceed" style="text-decoration: none;">
		<div class="alert alert-danger" style="width: 60%; margin-left: auto; margin-right: auto;">
			<strong><?= $msg ?></strong>
		</div>
	</a>
	<div>
		<div class="row ">
			<div class="col-sm theader" style="border-top-left-radius: 5px;">
				<?= __('Description') ?>
		    </div>
		    <div class="col-sm theader">
		    	<?= __('Date Debut') ?>
		    </div>
		    <div class="col-sm theader">
		    	<?= __('Date Fin') ?>
		    </div>
		    <div class="col-sm theader" style="border-top-right-radius: 5px;">
		      <?= 'Montant'  ?>
		    </div>
		</div>
		<div class="row">
			<div class="col-sm tcell">
				<?= __('Abonnement Annuel') ?>
		    </div>
		    <div class="col-sm tcell">
		    	<?= $start_date; ?>
		    </div>
		    <div class="col-sm tcell">
		    	<?= $due_date ?>
		    </div>
		    <div class="col-sm tcell amount">
		      <?= $amount.' &euro; '  ?>
		    </div>
		</div>
	</div>
	<div class="row paypal" style="text-align: center;margin-top: 40px;">
		<?= $this->Html->image('paypal-payments.png',['url'=>['action'=>'pay']]); ?>
	</div>
</div>
<style type="text/css">
	.paypal>a{
		margin-left: auto;
		margin-right: auto;
	}
	.paypal img{
		width: 250px;
	}
	#payment-container{
		margin-left: auto;
		margin-right: auto; 
		margin-top: 30px; 
		text-align: center;
		padding: 40px;
	}
	.theader{
		border: 1px solid lightgrey;
		font-weight: bold;
		padding: 10px;
		text-align: center;
		background-color: #e8e8e8;
		font-size: 20px;
	}
	.tcell{
		border-bottom: 1px solid lightgrey;
		border-left: 1px solid lightgrey;
		border-right: 1px solid lightgrey;
		padding: 20px;
		font-size: 18px;
		background-color: #fff;
	}
	.amount{
		font-size: 24px;
		font-weight: bolder;
	}
</style>