<!DOCTYPE html>
<html>
<?php //print_r($invoice); ?>
	<head>
		<title><?= __('Facture') ?></title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
	    <?= $this->Html->script(['jquery-3.3.1.js']); ?>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
	</head>
	<body>
		<div class="container-fluid">
			<?php $currency = ($invoice->currency === 'EUR')? " ".'&euro;' : " ".$invoice->currency  ?>
			<div class="row">
			    <div class="col-md-10" id="printable" style="margin-left: auto;margin-right: auto;margin-top: 30px; margin-bottom: 20px; border: 2px solid lightgrey; padding-left: 40px;padding-right: 40px;padding-bottom: 40px;">
			    	<div class="print"><i class="fas fa-print"></i></div>
			    	<br/><br/>
			    	<div class="invoice-header">
			    		<?= $this->Html->image("logo.jpg", [
		                "alt" => "The Concept Store International"]);?>
			            <div style="float: right;">
			            	<h1>Facture</h1>
			            	<div>
			            		<b><?= __('Numéro de la Facture') ?>: </b> <a href="#"><?= $invoice->transaction_id; ?></a>
			            	</div>
			            	<div><b><?= __('Date de Facturation') ?>: </b><?= $invoice->create_time->i18nFormat('dd/MM/yyyy'); ?></div>
				    	</div>
			    	</div>
			    	<div style="margin-top: 40px;">
			    		<div style="display: inline-block;">
			    			<h4><b>TheConceptStoreInternational</b></h4>
			    			<div>Mr Olivier DELESALLE</div>
			    			<div>Av. 4éme DM Route de Targa</div>
			    			<div>Residance Khalid, 1er étage, App. n°4</div>
			    			<div>40 000 - Marrakech</div>
			    			<div>+212-677354799</div>
			    			<div> 
			    				<a href="mailto:<?= "contact@comingoutstudio.com" ?>"><?= "contact@comingoutstudio.com" ?></a> 
			    			</div>
			    			<div>
			    				<a href="https://www.theconceptstoreinternational.com">https://www.theconceptstoreinternational.com</a>
			    			</div>
			    		</div>
			    		<div style="float: right;display: inline-block; border: 2px solid lightgrey; margin-right: 20px; padding-top: 5px;padding-bottom: 5px;padding-left: 50px;padding-right: 50px;text-align: center;">
			    			<h3><b><?= __('Montant') ?></b></h3>
			    			<h5><b><?= $invoice->total.$currency ?></b></h5>
			    		</div>
			    	</div>
			    	<br/><br/><br/><br/>
			    	<div>
			    		<h4><b><?= $invoice->firstname." ".$invoice->lastname ?></b></h4>
			    		<div>ID: <a href="#"><?= $invoice->payer ?></a></div>
			    		<div><?= $invoice->address ?></div>
			    		<div><?= $invoice->city.", ".$invoice->country_code ?></div>
			    		<div> <a href="mailto: <?= $invoice->email; ?>"><?= $invoice->email; ?></a></div>
			    	</div>
			    	<br/><br/><br/>
			    	<div class="row">
			    		<div class="col-sm theader">
					      <?= __('Description') ?>
					    </div>
					    <div class="col-sm theader">
					      <?= __('Période') ?>
					    </div>
					    <div class="col-sm theader">
					      <?= __('Prix') ?>
					    </div>
			    	</div>
			    	<div class="row" style="text-align: center;">
			    		<div class="col-sm tcell">
					      <?= __('Abonnement Annuel sur le site') ?>
					    </div>
					    <div class="col-sm tcell">
					    	<?= $invoice->create_time->i18nFormat('dd/MM/yyyy')." - ".$invoice->due_time->i18nFormat('dd/MM/yyyy'); ?>
					    </div>
					    <div class="col-sm tcell">
					    	<?= $invoice->total.$currency  ?>
					    </div>
			    	</div>
			    	<div class="row">
			    		<div class="col-sm">
					    </div>
					    <div class="col-sm total">
					    	<?= __('Total') ?>
					    </div>
					    <div class="col-sm total">
					      <?= $invoice->total.$currency  ?>
					    </div>
			    	</div>
			    </div>
			</div>
		</div>
	</body>
	<style type="text/css">
	.theader{
		border: 1px solid lightgrey;
		font-weight: bold;
		padding: 10px;
		text-align: center;
		background-color: #e8e8e8;
	}
	.tcell{
		border-bottom: 1px solid lightgrey;
		border-left: 1px solid lightgrey;
		border-right: 1px solid lightgrey;
		padding: 20px;
	}
	.total{
		text-align: center;
		padding-top: 20px;
		padding-bottom: 20px;
		border-bottom: 1px solid lightgrey;
		border-left: 1px solid lightgrey;
		border-right: 1px solid lightgrey;
		font-weight: bold;
		font-size: 18px;
	}
	.print{
	    position: absolute;
	    right: 0;
	    transition: opacity 0.3s ease-out;
	    background-color: #000;
	    opacity: 0;
	    padding-left: 20px;
	    padding-right: 20px;
	    padding-top: 5px;
	    padding-bottom: 5px;
	    font-size: 22px;
	    margin-top: -2px;
	    margin-right: -2px;
	    border-bottom-left-radius: 5px;
	    color: #fff;
	}
	.col-md-10:hover>.print{
		opacity: 0.7;
		cursor: pointer;
	}
	</style>
	<script type="text/javascript">
		    $(document).on('click','.print', function(){
		    	$('.print').hide();
		    	window.print();
		    	$('.print').show();
		    })
	</script>
</html>