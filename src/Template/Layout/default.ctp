<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css(['base.css','flexslider','owl.carousel.min','owl.theme.default','flags','dd']) ?>
    <?= $this->Html->script(['jquery-3.3.1.js','custom.js','jquery.flexslider','jquery.popupoverlay.js','jquery.form.min','jquery.cropit.js','owl.carousel.js','tinymce/tinymce.min']); ?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</head>
<body style="background-color: #000;">
	<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #000; display: block">
  <div class="collapse navbar-collapse">
  	<?php if (!empty($authenticated)): ?>
  	<ul class="navbar-nav ml-auto" style="margin-right: 40px;">
        <li class="nav-item ">
            <?= $this->Html->link((!empty($authenticated['company']))? $authenticated['company'] : $authenticated['firstname'].' '.$authenticated['lastname'], ['controller'=>'Users', 'action'=>'profile'],['class'=>'nav-link']) ?>
            <!--dropdown -> <a class="nav-link dropdown-toggle" href="#" id="authUserDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?= (!empty($authenticated['company']))? $authenticated['company'] : $authenticated['firstname'].' '.$authenticated['lastname'];?>
            </a>
            <div class="dropdown-menu bg-dark" aria-labelledby="authUserDropdown" style="color: #fff">
                <?= $this->Html->link(__('Profile'), ['controller'=>'Users', 'action'=>'profile'], ['class'=>'dropdown-item']); ?>
                <?= $this->Html->link(__('Paramètres'), ['controller'=>'Users', 'action'=>'settings'], ['class'=>'dropdown-item']); ?>
            </div> -->
        </li>
        <li class="nav-item">
            <?= $this->Html->link(__('Se Déconnecter'), ['controller'=>'Users', 'action'=>'logout'],['class'=>'nav-link']) ?>
        </li>
    </ul>
  	<?php else: ?>
    <ul class="navbar-nav ml-auto" style="margin-right: 40px;">
        <li class="nav-item">
            <a class="nav-link login_open" href="#"><?=__('Se Connecter') ?></a>
        </li>
        <li></li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              S'Enregistrer
            </a>
            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownMenuLink" style="color: #fff">
              <a class="dropdown-item register_open" href="#"><?= __('Inscription') ?></a>
              <a class="dropdown-item register-press_open" href="#"><?= __('Inscription Press') ?></a>
            </div>
        </li>
    </ul>
    <?php endif ?>
  </div>
    <!-- <ul class="navbar-nav ml-auto" style="margin-right: 50px;float: right;">
        <li style="margin-left: 2px; margin-right: 2px;"><?= $this->Html->image("flags/en.png");?></li>
        <li style="margin-left: 2px; margin-right: 2px;"><?= $this->Html->image("flags/fr.png");?></li>
        <li style="margin-left: 2px; margin-right: 2px;"><?= $this->Html->image("flags/es.png");?></li>
    </ul> -->
    <?php if (!empty($authenticated)): ?>
    <div class="search-div">
        <button class="btn btn-dark search-btn">
            Recherche par Pays
            <div style="position: absolute; z-index: 99; width: 350px; text-align: left; display: none;">
                <select class="countries search" id="search-by-count" size="5">
                    <option value="" selected>(Choisissez un Pays)</option>
                  <?php foreach ($countries as $country) :?>
                    <?php if (isset($cntry) && $cntry==$country->id): ?>
                        <option value='<?= $country->id; ?>' data-image="/img/blank.gif" data-imagecss="flag <?= $country->code; ?>" data-title="<?= $country->name; ?>" selected>
                            <?= $country->name; ?>
                        </option>
                    <?php else: ?>
                        <option value='<?= $country->id; ?>' data-image="/img/blank.gif" data-imagecss="flag <?= $country->code; ?>" data-title="<?= $country->name; ?>">
                            <?= $country->name; ?>
                        </option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </select>
            </div>
        </button>
    <button class="btn btn-dark search-btn">
        Recherche par Categorie
        <div style="position: absolute; z-index: 99; text-align: left; display: none;">
                <select class="categories search" id="search-by-cat" size="5">
                    <option value="" selected>(Choisissez une Categorie) </option>
                    <?php foreach ($categoriespress as $catpress): ?>
                        <option value='<?= $catpress->id; ?>' <?= (isset($cat) && $cat==$catpress->id)? 'selected' : '' ?>>
                            <?= $catpress->name; ?>
                        </option>
                    <?php endforeach ?>
                  <?php foreach ($categories as $category) :?>
                        <option value='<?= $category->id; ?>' <?= (isset($cat) && $cat==$category->id)? 'selected' : '' ?>>
                            <?= $category->name; ?>
                        </option>
                  <?php endforeach; ?>
                </select>
            </div>
    </button>
    </div>
    <?php endif ?>
</nav>

<div class="container-fluid" style="height: auto; min-height: calc(100% - 140px)">
    <div class="col-md-9 profile-container">
    <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #000;">
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav">
            <li class="nav-item active">
            <?= $this->Html->image("logo.jpg", [
                "alt" => "The Concept Store International",
                "url" => ['controller' => 'Pages', 'action' => 'display', 'home']
            ]);?>
          </li>
        </ul>
        <!--<br/>
        <ul class="navbar-nav" style="margin-top: 20px;margin-left: 20px;">
	        <li class="nav-item">
	          	<?= $this->Html->link(__('Qui Somme Nous'),['controller'=>'Pages','action'=>'aboutus'],['class'=>'nav-link']) ?>
	        </li>
	        <li class="nav-item"> ___ </li>
            <li class="nav-item"> 
                <a href="#" class="nav-link mentionlegal_open"><?= __('Mention Légal')  ?></a>
            </li>
          	<li class="nav-item"> ___ </li>
          	<li class="nav-item">
			<?= $this->Html->link(__('Contact'),['controller'=>'Pages','action'=>'contact'],['class'=>'nav-link']) ?>
			</li>
        </ul>-->
      </div>
    </nav>
    </div>
    <?= $this->fetch('content') ?>
    <?php if (empty($authenticated)): ?>
    <!-- ================================ INSCRIPTION ============================== -->
    <div id="register" style="display: none;">
        <div class="close-header" style="text-align: right;">
            <i class="fa fa-times register_close"></i>
        </div>
        <div class="content">
            <div class="alert alert-danger emailexists"><?= __("l'email que vous avez entré existe déjà") ?></div>
            <form id="register-form">
                <div style="padding-left: 5%;margin-bottom: 10px;">
                    <!--<input type="radio" class="usertype" name="type" value="artist" checked />
                    <span style="font-size: 20px; margin-left: 10px;">
                        <?= __('Personnel') ?>
                    </span> -->
                    <input type="radio" class="usertype ste" name="type" value="company" style="margin-left: 10px;" />
                    <span style="font-size: 20px; margin-left: 10px;">
                        <?= __('Société') ?>
                    </span>
                    <!--<input type="radio" class="usertype" name="type" value="particular" style="margin-left: 10px;" />
                    <span style="font-size: 20px; margin-left: 10px;">
                        <?= __('Particulier') ?>
                    </span> -->
                </div>
                <div class="form-element company" style="display: none;">
                    <label for="company" class="required"><?= __('Société') ?></label>
                    <input type="text" name="company" class="req">
                </div>
                <div class="form-element artist particular">
                    <label for="lastname" class="required"><?= __('Nom') ?></label>
                    <input type="text" name="lastname" required>
                </div>
                <div class="form-element artist particular">
                    <label for="firstname" class="required"><?= __('Prénom') ?></label>
                    <input type="text" name="firstname" required>
                </div>
                <div class="form-element artist company">
                    <label for="category_id" class="required"><?= __('Catégorie') ?></label>
                    <select class="categories" name="category_id" class="req">
                        <?php foreach ($categories as $cat) :?>
                            <option value='<?= $cat->id; ?>' >
                                <?= $cat->name; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-element artist company">
                    <label for="presentation" class="required"><?= __('Présentation') ?></label>
                    <textarea name="presentation" rows="5" required></textarea>
                </div>
                <div class="form-element artist company particular">
                    <label for="country" class="required"><?= __('Pays') ?></label>
                    <select name="country_id" class="countries" name="country_id" class="req">
                      <?php foreach ($countries as $country) :?>
                        <?php if ($country->code == "fr"): ?>
                            <option value='<?= $country->id; ?>' data-image="/img/blank.gif" data-imagecss="flag <?= $country->code; ?>" data-title="<?= $country->name; ?>" selected>
                                <?= $country->name; ?>
                            </option>
                        <?php else: ?>
                            <option value='<?= $country->id; ?>' data-image="/img/blank.gif" data-imagecss="flag <?= $country->code; ?>" data-title="<?= $country->name; ?>">
                                <?= $country->name; ?>
                            </option>
                        <?php endif ?>
                      <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-element particular">
                    <label for="city" class="required"><?= __('Ville') ?></label>
                    <input type="text" name="city" required>
                </div>
                <div class="form-element artist company">
                    <label for="address"><?= __('Adresse') ?></label>
                    <textarea name="address" rows="4"></textarea>
                </div>
                <div class="form-element artist company">
                    <label for="zipcode"><?= __('Code Postal') ?></label>
                    <input type="number" name="zipcode">
                </div>
                <div class="form-element artist company particular">
                    <label for="phone" class="required"><?= __('Tél.') ?></label>
                    <input type="text" name="phone" required>
                </div>
                <div class="form-element artist company">
                    <label for="fax">Fax</label>
                    <input type="text" name="fax">
                </div>
                <div class="form-element artist company">
                    <label for="gsm">GSM</label>
                    <input type="text" name="gsm">
                </div>
                <div class="form-element artist company particular">
                    <label for="primaryemail" class="required">E-mail</label>
                    <input type="email" name="primaryemail" class="email req" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" placeholder="Principal" required>
                    <input type="email" name="secondaryemail" class="email req" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" placeholder="Secondaire">
                </div>
                <div class="form-element artist company particular">
                    <label for="password" class="required"><?= __('Mot de passe') ?></label>
                    <input type="password" name="password" class="req" required>
                </div>
                <div class="form-element artist company particular">
                    <label for="confirm_password" class="required"><?= __('Confirmer Mdp.') ?></label>
                    <input type="password" name="confirm_password" class="req" required>
                </div>
                <div class="form-element artist company">
                    <label for="website"><?= __('Site Web') ?></label>
                    <input type="text" name="website">
                </div>
                <div class="form-element artist company">
                    <label for="facebook">Facebook</label>
                    <input type="text" name="facebook">
                </div>
                <div class="form-element artist company">
                    <label for="linkedin">Linkedin</label>
                    <input type="text" name="linkedin">
                </div>
                <div class="form-element artist company">
                    <label for="twitter">Twitter</label>
                    <input type="text" name="twitter">
                </div>
                <div class="form-element artist company">
                    <label for="instagram">Instagram</label>
                    <input type="text" name="instagram">
                </div>
                <div class="form-element artist company">
                    <label for="pinterest">Pinterest</label>
                    <input type="text" name="pinterest">
                </div>
                <div class="gallery-text artist company"><?= __('Votre Gallerie') ?></div>
                <div class="separator artist company"></div>
                <div class="form-element artist company">
                    <?php for ($i=0; $i < 5; $i++):?>
                        <div class="up-block" style="padding: 10px;">
                            <i class="fa fa-camera" style="vertical-align: sub; font-size: 25px;"></i><span><input type="file" name="gallerie[]" class="gallerie-files" style="margin-left: 15px;"></span>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="form-element">
                    <button type="submit" class="submit-btn"><?= __("S'inscrire") ?></button>
                </div>
            </form>
            <div class="loader-container">
                <?= $this->Html->image('loading.gif',['class'=>'loader']);?>
                <p><?= __('Veuillez patientez') ?> ...</p>
            </div>
        </div>
    </div>
    <!-- ================================ FIN INSCRIPTION ============================== -->
    <!-- ================================ INSCRIPTION PRESS ============================== -->
    <style>

      #result {
        margin-top: 10px;
        width: 900px;
      }

      #result-data {
        display: block;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        word-wrap: break-word;
      }
    </style>
    <div id="register-press" style="display: none;">
        <div class="close-header" style="text-align: right;">
            <i class="fa fa-times register-press_close"></i>
        </div>
        <div class="content">
            <form id="registerpress-form" enctype="multipart/form-data">
                <div class="form-element">
                    <div class="image-editor">
                        <i class="fas fa-pencil-alt edit-img"></i>
                        <input type="file" accept="image/*" id="profile-pic" class="cropit-image-input" style="visibility: hidden;">
                        <div class="cropit-preview">
                        </div>
                        <div style="margin-top: 10px; display: none;" class="tools-container">
                            <i class="fa fa-image"></i><input type="range" class="cropit-image-zoom-input"><i class="fa fa-image" style="font-size: 25px;"></i>
                            <img src="/img/check.png" class="validate-img">
                        </div>
                    </div>
                </div>
                <br/>
                <div class="alert alert-danger pressemailexists"> <?= __("l'email que vous avez entre existe déjà") ?></div>
                <input type="hidden" name="profilepic" class="hidden-image-data" />
                <div class="form-element company">
                    <label for="company" class="required"><?= __('Société') ?></label>
                    <input type="text" name="company">
                </div>
                <input type="hidden" name="type" value="press" />
                <div class="form-element">
                    <label for="presentation" class="required"><?= __('Présentation') ?></label>
                    <textarea name="presentation" rows="5" required></textarea>
                </div>
                <div class="form-element">
                    <label for="country" class="required"><?= __('Pays') ?></label>
                    <select name="country_id" class="countries" style="width:300px;" name="country">
                      <?php foreach ($countries as $country) :?>
                        <?php if ($country->code == "fr"): ?>
                            <option value='<?= $country->id; ?>' data-image="/img/blank.gif" data-imagecss="flag <?= $country->code; ?>" data-title="<?= $country->name; ?>" selected>
                                <?= $country->name; ?>
                            </option>
                        <?php else: ?>
                            <option value='<?= $country->id; ?>' data-image="/img/blank.gif" data-imagecss="flag <?= $country->code; ?>" data-title="<?= $country->name; ?>">
                                <?= $country->name; ?>
                            </option>
                        <?php endif ?>
                      <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-element">
                    <label for="city" class="required"><?= __('Ville') ?></label>
                    <input type="text" name="city" required>
                </div>
                <div class="form-element">
                    <label for="address"><?= __('Adresse') ?></label>
                    <textarea name="address" rows="4"></textarea>
                </div>
                <div class="form-element">
                    <label for="zipcode"><?= __('Code Postal') ?></label>
                    <input type="number" name="zipcode">
                </div>
                <div class="form-element">
                    <label for="phone" class="required"><?= __('Tél.') ?></label>
                    <input type="text" name="phone" required>
                </div>
                <div class="form-element">
                    <label for="fax">Fax</label>
                    <input type="text" name="fax">
                </div>
                <div class="form-element">
                    <label for="gsm">GSM</label>
                    <input type="text" name="gsm">
                </div>
                <div class="form-element">
                    <label for="primaryemail" class="required">E-mail</label>
                    <input type="email" name="primaryemail" class="email" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" placeholder="Principal" required>
                    <input type="email" name="secondaryemail" class="email" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" placeholder="Secondaire">
                </div>
                <div class="form-element">
                    <label for="password" class="required"><?= __('Mot de passe') ?></label>
                    <input type="password" name="password">
                </div>
                <div class="form-element">
                    <label for="confirm_password" class="required"><?= __('Confirmer Mdp.') ?></label>
                    <input type="password" name="confirm_password">
                </div>
                <div class="form-element">
                    <label for="website">Site Web</label>
                    <input type="text" name="website">
                </div>
                <div class="form-element">
                    <label for="facebook">Facebook</label>
                    <input type="text" name="facebook">
                </div>
                <div class="form-element">
                    <label for="linkedin">Linkedin</label>
                    <input type="text" name="linkedin">
                </div>
                <div class="form-element">
                    <label for="twitter">Twitter</label>
                    <input type="text" name="twitter">
                </div>
                <div class="form-element">
                    <label for="instagram">Instagram</label>
                    <input type="text" name="instagram">
                </div>
                <div class="form-element">
                    <label for="pinterest">Pinterest</label>
                    <input type="text" name="pinterest">
                </div>
                <br/>
                <div class="form-element">
                    <button type="submit" class="submit-btn"><?=__("S'inscrire") ?></button>
                </div>
            </form>
            <div class="loaderpress-container">
                <?= $this->Html->image('loading.gif',['class'=>'loader']);?>
                <p>Veuillez patientez ...</p>
            </div>
        </div>
    </div>
    <!-- ================================ FIN INSCRIPTION PRESS ============================== -->
    <!-- =================================== LOGIN FORM ================================ -->
    <div id="login" style="display: none;">
        <div class="close-header" style="text-align: right;">
            <i class="fa fa-times login_close"></i>
        </div>
        <div class="content">
            <form class="login-form">
                <div class="alert alert-danger" id="loginerror">
                    <?= __("Nom d'utilisateur ou Mot de passe incorrect") ?>
                </div>
                <div class="form-element" style="margin-bottom: 10px;">
                    <input type="email" name="primaryemail" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" style="width: 95%; height: 40px;" placeholder="E-mail" required>
                </div>
                <div class="form-element">
                    <input type="password" name="password" style="width: 95%;height: 40px;" placeholder="Password">
                </div>
                <div class="form-element" style="text-align: center;font-size: 12px;margin-bottom: 10px;">
                    <a href="#"><?= __('Mot de passe Oublié?') ?></a>
                </div>
                <div id="loading" style="text-align: center; display: none;">
                    <?= $this->Html->image('loading.gif',['style'=>'width: 30px;']);?>
                </div>
                <div class="form-element" style="margin-bottom: 20px;">
                    <button type="submit" class="submit-btn" style="width: 95%;margin-left: 0;"><?= __('Se Connecter') ?></button>
                </div>
            </form>
        </div>
    </div>
    <!-- ================================= LOGIN FORM END ============================== -->
    <?php endif; ?>
    <!-- =================================== Mention Legal ================================ -->
    <div id="mentionlegal">
        <div class="close-header" style="text-align: right;">
            <i class="fa fa-times mentionlegal_close"></i>
        </div>
        <div class="content" style="color: #fff !important;">                
            <div class="txt">
                <h6>MENTIONS LÉGALES</h6>
                COMING OUT <br/>
                Numéro IF : 20695662<br/>
                Numéro Patente : 64685752<br/>
                Numéro ICE : 001709034000049<br/>
                Editeur COMING OUT <br/>
                Au capital de 100.000 Dhs<br/>
                Immatriculée au RCS de Marrakech sous le n°  77263<br/>
                Dont le siège social est situé Av Cheibh Rabhi 4éme DM Route de Targa Résidence Khalid 1 er étage App. N°4 Guéliz 40000 Marrakech <br/>
                Téléphone : 00.212.5.24.43.66.10<br/>
                Pour nous contactez :  
                <ul>
                    <li>
                        Directeur de la publication : Faiçal Bnouaadi (direction@conceptstoreinternational.com) 
                    </li>
                    <li>
                        Directeur Marketing & Communication : Olivier Delesalle : (marketing@conceptstoreinternational.com, commercial@conceptstoreinternational.com)
                    </li>
                </ul>

                OD Production & Communication
                <ul>
                    <li>Numéro IF : 06525929</li>
                    <li>Numéro Patente : 45192789</li>
                    <li>Numéro ICE : 000227840000060</li>
                </ul>
                <p> 
                Au capital de 100.000 Dhs <br/>
                Immatriculée au RCS de Marrakech sous le n° 49097 Dont le siège social est situé 3, Rue de Yougoslavie, Résidence Andalous Guéliz 40000 Marrakech<br/>
                Téléphone : 00.212.6.77.35.47.99<br/>
                Hébergeur : COMING OUT <br/>
                </p>

                <h6>PROPRIETÉ INTELLECTUELLE</h6>
                <p>
                La Société Editrice de COMING OUT  est le propriétaire exclusif de tous les droits de propriété intellectuelle portant tant sur la structure que sur le contenu 
                du Site <a href="http://www.theconceptstoreinternational.com" _blank>http://www.theconceptstoreinternational.com </a> 
                </p>
                <p>
                Les contenus du site  <a href="http://www.theconceptstoreinternational.com" _blank>http://www.theconceptstoreinternational.com </a> sont destinés à usage strictement personnel, privé et non collectif, librement pour le 
                contenu gratuit, en contrepartie d’un abonnement pour le contenu payant. Il en est de même pour les fils RSS et les newsletters.
                </p>
                <p>
                Toute utilisation dans un cadre professionnel ou commercial ou toute commercialisation de ce contenu auprès de tiers est interdite, sauf accord exprès de la Société Editrice de OUMING OUT à demander à l’adresse suivante : <b>direction@conceptstoreinternational.com</b> 
                </p>
            </div>
        </div>
    </div>
    <!-- ================================= Mention Legal END ============================== -->
</div>
<div class="row footer">
		<div class="col-sm-3">
	      
	    </div>
	    <div class="col-sm">
	        <nav class="navbar navbar-expand-lg navbar-dark" style="padding: 0">
		        <div class="collapse navbar-collapse">
    		        <ul class="navbar-nav ml-auto">
    		            <li class="nav-item">
    		          	   <?= $this->Html->link(__('Qui Somme Nous'),['controller'=>'Pages','action'=>'aboutus'],['class'=>'nav-link']) ?>
    		            </li>
    		            <li class="nav-item"> ___ </li>
                        <li class="nav-item"> 
                            <a href="#" class="nav-link mentionlegal_open"> <?= __('Mention Légal') ?> </a>
                        </li>
    		          <li class="nav-item"> ___ </li>
    		          <li class="nav-item">
    		          	<?= $this->Html->link(__('Contact'),['controller'=>'Pages','action'=>'contact'],['class'=>'nav-link']) ?>
    		          </li>
                      <li class="nav-item"> ___ </li>
                      <li class="nav-item">
                        <?= $this->Html->link(__('Presse'),['controller'=>'Pages','action'=>'press'],['class'=>'nav-link']) ?>
                      </li>
    		        </ul>
		        </div>
		    </nav>
	    </div>
	    <div class="col-sm" style="text-align: center;">
	    	<form id="newsletter-form">
		      	<span class="newsletterTxt"><?=__('Newsletter') ?></span>
		      	<div class="input-group newsletter" style="width: 70%; max-width: 260px;display: inline-flex;">
			        <input type="email" class="form-control newsletterInput" name="email" placeholder="E-mail" required>
			        <div class="input-group-prepend">
			          <button type="submit" class="input-group-text"><i class="fa fa-envelope"></i></button>
			        </div>
			    </div>
		    </form>
	    </div>
	</div>
    <script src="/js/jquery.dd.min.js"></script>
    <script>
      $(".countries").msDropdown();
      $('.categories').msDropdown({childWidth:"250px",enableCheckbox:true});
      $( document ).ready(function() {
		    $(".ste").click();
		});
    </script>
</body>
</html>
