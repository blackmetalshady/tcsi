<?php 
$title = (!empty($user['company']))? $user['company'] : $user['firstname']." ".$user['lastname'];
 ?>
<?php $this->assign('title','TCSI - Profile de '.$title); ?>
<?php //print_r($user);die(); ?>
<div class="col-md-9 profile-container">
<div class="row" style="background-color: #000;padding-left: 38px;-moz-user-select: none;-webkit-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none;">
    <div class="col-md-3" style="padding: 0; max-width: 270px;">
        <div class="img-container">
            <?php if($session_id == $user['id']): ?>
            <div class="edit-pic edit_pic_open">
                <i class="fas fa-camera"></i>
            </div>
            <?php endif ?>
            <img src="<?= (!empty($user['profilepic']))? $user['profilepic'] : '/img/default.jpg'; ?>" style="max-width: 250px" alt="">
        </div>
    </div>
    <div class="col-md editable" style="color: #fff; min-height: 20px;margin-left: 30px;">
        <?php if($session_id == $user['id']): ?>
            <div class="edit">
                <a href="#" class="lnk edit_open"><?= __("Modifier"); ?></a>
            </div>
        <?php endif ?>
        <h2><?= (!empty($user['company']))? $user['company'] : $user['firstname']." ".$user['lastname']?></h2>
        <h3><?= $user['category']['name']?></h3>
        <ul class="fa-ul" style="margin-left: 25px;">
            <li>
                <span class="fa-li" >
                    <i class="fas fa-phone"></i>
                </span>
                <div class="list-label">Tel.</div>
                <div style="display: inline-block;">: <?= $user['phone']?></div>
            </li>
            <li>
                <span class="fa-li" >
                    <i class="fas fa-at"></i>
                </span>
                <div class="list-label">E-mail</div>
                <div style="display: inline-block;">: <?= $user['primaryemail']?></div>
            </li>
            <li>
                <span class="fa-li" >
                    <i class="fas fa-globe"></i>
                </span>
                <div class="list-label">Site</div>
                <div style="display: inline-block;">: 
                    <?php
                        if (!empty($user['website'])) {
                            $user['website'] = (strpos($user['website'], 'http') !== false)? $user['website'] : 'http://'.$user['website'];
                        }
                        
                        echo $this->Html->link($user['website'], $user['website'],['target'=>'_blank', 'class'=>'lnk']);?>
                </div>
            </li>
            <li>
                <span class="fa-li" >
                    <i class="fab fa-facebook-f"></i>
                </span>
                <div class="list-label">Facebook</div>
                <div style="display: inline-block;">: <?= $this->Html->link($user['facebook'], $user['facebook'],['target'=>'_blank', 'class'=>'lnk']);?></div>
            </li>
            <li>
                <span class="fa-li" >
                    <i class="fab fa-twitter"></i>
                </span>
                <div class="list-label">Twitter</div>
                <div style="display: inline-block;">: <?= $this->Html->link($user['twitter'], $user['twitter'],['target'=>'_blank', 'class'=>'lnk']);?></div>
            </li>
            <li>
                <span class="fa-li" >
                    <i class="fab fa-instagram"></i>
                </span>
                <div class="list-label">Instagram</div>
                <div style="display: inline-block;">: <?= $this->Html->link($user['instagram'], $user['instagram'],['target'=>'_blank', 'class'=>'lnk']);?></div>
            </li>
            <li>
                <span class="fa-li" >
                    <i class="fab fa-pinterest"></i>
                </span>
                <div class="list-label">Pinterest</div>
                <div style="display: inline-block;">: <?= $this->Html->link($user['pinterest'], $user['pinterest'],['target'=>'_blank', 'class'=>'lnk']);?></div>
            </li>
        </ul>
    </div>
</div>
<div class="row editable" style="color: #fff;padding-left: 38px;margin-top: 30px; margin-bottom: 30px;">
    <?php if($session_id == $user['id']): ?>
        <div class="edit">
            <a href="#" class="lnk edit_presentation_open"><?= __("Modifier"); ?></a>
        </div>
    <?php endif ?>   
    <h2><?= __("Présentation") ?></h2>
    <div class="presentation_content" style="width: 100%;">
        <?= $user['presentation'];?>
    </div>
</div>
<?php if (!empty($user->pictures)): ?>  
<div class="row editable" style="color: #fff;padding-left: 38px;">
    <?php if($session_id == $user['id']): ?>
            <div class="edit">
                <a href="#" class="lnk gallery_pic_open"><?= __("Modifier"); ?></a>
            </div>
        <?php endif ?>
    <div class="owl-carousel owl-theme profile_carousel" style="margin-top: 35px;">
        <?php foreach ($user->pictures as $pic): ?>
        <div class="item slider_open" class="gallerie-pic">
            <?= $this->Html->image('gallerie/'.$user['id'].'/'.$pic->filename,['class'=>'gallerie-pic']); ?>
        </div>          
        <?php endforeach; ?>
        <!--<div class="item" style="width: 200px; height: 200px; position: relative;color: #000; background-color: #fff;">
            <div style="position: absolute; top: 50%; left: 50%;transform: translate(-50%, -50%);">
                <div style="text-align: center;">
                    <div class="btn btn-primary" >
                        <span class="fa fa-plus" onclick="$('#upload').click()"></span>
                        <?= $this->Form->create(null, ['type' => 'file', 'id'=>'imgup' ,'url' => ['controller'=>'Medias','action' => 'upload']]);?>
                        <?= $this->Form->input('img', [
                            'type' => 'file',
                            'accept'=>'image/*',
                            'id'=>'upload',
                            'onchange'=>'',
                            'style'=>'display: none;',  
                            'label'=>false
                        ]);?>
                        <?= $this->Form->submit('envoyer', ['id'=>'sbmt-btn']);?>
                        <?= $this->Form->end();?>
                    </div>
                </div>
                <div style="text-align: center;">Ajouter</div>
                <div id="progress" >
                    <div id="bar"></div>
                    <center style="position: relative;"><div id="percent">0%</div></center>
                </div>
            </div>
        </div> -->
    </div>
</div>
<?php else: ?>
    <div style="text-align: center;color: #fff;padding-top: 50px; font-size: 18px">
        Vous n'avez pas encore de photos dans votre gallerie, <a href="#" class="lnk gallery_pic_open">Ajoutez des photos</a>
    </div>  
<?php endif ?>
</div>
    <!-- ================================ Edit Profile ============================== -->
    <div id="edit" style="display: none;">
        <div class="close-header" style="text-align: right;">
            <i class="fa fa-times edit_close"></i>
        </div>
        <div class="content">
            <form id="edit-form">
                <?php if ($user['type'] == 'company' || $user['type'] == 'press'): ?>
                <div class="form-element">
                    <label for="company" class="required"><?= __('Société') ?></label>
                    <input type="text" name="company" value="<?= $user['company']?>" required>
                </div>
                <?php else: ?>
                <div class="form-element">
                    <label for="firstname" class="required"><?= __('Prénom') ?></label>
                    <input type="text" name="firstname" value="<?= $user['firstname']?>" required>
                </div>
                <div class="form-element">
                    <label for="lastname" class="required"><?= __('Nom') ?></label>
                    <input type="text" name="lastname" value="<?= $user['lastname']?>" required>
                </div>
                <?php endif ?>
                <?php if (!empty($user['category_id']) && $user['category_id'] != '7cf5c1cd-feaa-4ac1-b7e1-5feb69993c50'): ?>
                <div class="form-element">
                    <label for="category_id" class="required"><?= __('Catégorie') ?></label>
                    <select class="categories" name="category_id">
                        <?php foreach ($categories as $cat) :?>
                            <option value='<?= $cat->id; ?>' <?= ($user['category_id'] == $cat->id)? 'selected' : ''?>>
                                <?= $cat->name; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <?php endif ?>
                <div class="form-element">
                    <label for="primaryemail" class="required">E-mail</label>
                    <input type="email" name="primaryemail" value="<?= $user['primaryemail']?>" class="email" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" placeholder="Principal" required>
                    <input type="email" name="secondaryemail" value="<?= $user['secondaryemail']?>"class="email" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" placeholder="Secondaire">
                </div>
                <div class="form-element">
                    <label><?= __('Mot de Passe') ?></label>
                    <a href="#" class="change_pass_open edit_close">(Modifier votre Mot de Passe)</a>
                </div>
                <div class="form-element">
                    <label for="country" class="required"><?= __('Pays') ?></label>
                    <select name="country_id" class="countries" style="width:300px;" name="country">
                      <?php foreach ($countries as $country) :?>
                            <option value='<?= $country->id; ?>' data-image="/img/blank.gif" data-imagecss="flag <?= $country->code; ?>" data-title="<?= $country->name; ?>" <?php if($user['country_id'] == $country->id){ echo "selected";} ?> >
                                <?= $country->name; ?>
                            </option>
                      <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-element">
                    <label for="city" class="required"><?= __('Ville') ?></label>
                    <input type="text" name="city" value="<?= $user['city']?>" required>
                </div>
                <div class="form-element">
                    <label for="address"><?= __('Adresse') ?></label>
                    <textarea name="address" value="<?= $user['address']?>" rows="4"></textarea>
                </div>
                <div class="form-element">
                    <label for="zipcode"><?= __('Code Postal') ?></label>
                    <input type="number" value="<?= $user['zipcode']?>" name="zipcode">
                </div>
                <div class="form-element">
                    <label for="phone" class="required"><?= __('Tél.') ?></label>
                    <input type="text" name="phone" value="<?= $user['phone']?>" required>
                </div>
                <div class="form-element">
                    <label for="fax">Fax</label>
                    <input type="text" value="<?= $user['fax']?>" name="fax">
                </div>
                <div class="form-element">
                    <label for="gsm">GSM</label>
                    <input type="text" value="<?= $user['gsm']?>" name="gsm">
                </div>
                <div class="form-element">
                    <label for="website">Site Web</label>
                    <input type="text" value="<?= $user['website']?>" name="website">
                </div>
                <div class="form-element">
                    <label for="facebook">Facebook</label>
                    <input type="text" value="<?= $user['facebook']?>" name="facebook">
                </div>
                <div class="form-element">
                    <label for="linkedin">Linkedin</label>
                    <input type="text" value="<?= $user['linkedin']?>" name="linkedin">
                </div>
                <div class="form-element">
                    <label for="twitter">Twitter</label>
                    <input type="text" value="<?= $user['twitter']?>" name="twitter">
                </div>
                <div class="form-element">
                    <label for="instagram">Instagram</label>
                    <input type="text" value="<?= $user['instagram']?>" name="instagram">
                </div>
                <div class="form-element">
                    <label for="pinterest">Pinterest</label>
                    <input type="text" value="<?= $user['pinterest']?>" name="pinterest">
                </div>
                <div class="alert alert-danger" style="display: none;text-align: center;font-weight: bold;max-width: 90%;margin: auto;" id="email-exists">
                    L'email principal que vous avez saisie exist deja,veuillez choisir un autre
                </div>
                <div class="loader-container">
                    <?= $this->Html->image('loading.gif',['class'=>'loader']);?>
                    <p>Veuillez patientez ...</p>
                </div>
                <br/>
                <div class="form-element">
                    <button type="submit" class="submit-btn"><?=__("Mettre a jour") ?></button>
                </div>
            </form>
        </div>
    </div>
    <!-- ================================ FIN Edit ============================== -->
    <!-- ================================ Update Description ============================== -->
    <div id="edit_presentation" style="display: none;">
        <div class="close-header" style="text-align: right;">
            <i class="fa fa-times edit_presentation_close"></i>
        </div>
        <div class="content">
            <h2><?= __("Présentation") ?></h2>
            <div class="form-element">
                <textarea name="presentation" id="tinyarea" rows="5" required>
                    <?= $user['presentation']; ?>
                </textarea>
            </div>
            <div id="loading" style="text-align: center; display: none;">
                <?= $this->Html->image('loading.gif',['style'=>'width: 30px;']);?>
            </div>
            <div class="form-element">
                <button type="submit" class="submit-btn presentation"><?=__("Mettre a jour") ?></button>
            </div>
        </div>
    </div>
    <!-- ================================ FIN Update Description ============================== -->
    <!-- ================================ Edit Photo ============================== -->
    <div id="edit_pic" style="display: none;">
        <div class="close-header" style="text-align: right;">
            <i class="fa fa-times edit_pic_close"></i>
        </div>
        <div class="content">
                <div class="form-element">
                    <div class="image-editor">
                        <i class="fas fa-pencil-alt edit-img"></i>
                        <input type="file" accept="image/*" id="profile-pic" class="cropit-image-input" style="visibility: hidden;">
                        <div class="cropit-preview" style="background: url(<?= $user['profilepic'];?>) no-repeat center center;">
                        </div>
                        <div style="margin-top: 10px; display: none;" class="tools-container">
                            <i class="fa fa-image"></i><input type="range" class="cropit-image-zoom-input"><i class="fa fa-image" style="font-size: 25px;"></i>
                            <img src="/img/check.png" class="validate-img">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="profilepic" class="hidden-image-data" />
                <div id="loading" style="text-align: center; display: none;">
                    <?= $this->Html->image('loading.gif',['style'=>'width: 30px;']);?>
                </div>
                <div class="alert alert-danger alert-pic-empty"><?= __("Veuillez validez votre photo") ?></div>
                <div class="form-element">
                    <button type="submit" class="submit-btn profilepic"><?=__("Mettre a jour") ?></button>
                </div>
        </div>
    </div>
    <!-- ================================ FIN Edit Photo ============================== -->
    <!-- ================================ Change Password ============================== -->
    <div id="change_pass" style="display: none;">
        <div class="content">
                <div class="form-element">
                    <label for="old_pass">Ancien Mot de Passe</label>
                    <input type="password" name="old_pass" id="old_pass" minlength=6>
                </div>
                <div class="form-element">
                    <label for="new_pass">Nouveau Mot de Passe</label>
                    <input type="password" name="new_pass" id="new_pass" minlength=6>
                </div>
                <div class="form-element">
                    <label for="confirm_new_pass">Confirmer le Mot de Passe</label>
                    <input type="password" name="confirm_new_pass" id="confirm_new_pass" minlength=6>
                </div>
                <div class="alert alert-success alert-pass" id="changepass-success">
                    Votre mot de passe a ete mis a jour avec succes, Veuillez vous reconnecter
                </div>
                <div class="alert alert-danger alert-pass" id="changepass-err"></div>
                <div id="loading" style="text-align: center; display: none;">
                    <?= $this->Html->image('loading.gif',['style'=>'width: 30px;']);?>
                </div>
                <div class="form-element">
                    <button type="submit" class="submit-btn change-pass"><?=__("Mettre a jour") ?></button>
                </div>
        </div>
    </div>
    <!-- ================================ FIN Change Password ============================== -->
    <!-- ================================ Edit Photo ================================== -->
    <div id="slider" style="max-width: 70%;">
        <div class="flexslider" style="overflow: hidden;">
          <ul class="slides">
            <?php foreach ($user->pictures as $pic): ?>
            <li>
              <?= $this->Html->image('gallerie/'.$user['id'].'/'.$pic->filename,['class'=>'']);?>
            </li>
            <?php endforeach ?>
          </ul>
        </div>
    </div>
    <!-- ================================ FIN Edit Photo ============================== -->
    <!-- ================================ Edit Gallery ================================== -->
    <div id="gallery_pic" class="content" style="display: none;">
        <div class="loader-container">
            <?= $this->Html->image('loading.gif',['class'=>'loader']);?>
            <p>Veuillez patientez ...</p>
        </div>
        <?php if (empty($user->pictures )): ?>
            <div id="add-gal">
                <i class="fas fa-plus"></i>
                <div style="font-size: 20px;">Ajoutez des Photos</div>
            </div>
        <?php else: ?>
            <div class="grid-container">
                <?php 
                foreach ($user->pictures as $pic) { 
                    echo '<div class="grid-item" id="'.$pic->id.'">
                                <div class="thumbnail">'.$this->Html->image('gallerie/'.$user['id'].'/'.$pic->filename).'<i class="fas fa-times del-pic" data-pic="'.$pic->filename.'"></i></div>
                                
                          </div>';
                } ?>
                <div class="grid-item" style="border: 2px dashed #949292;">
                    <div class="add-thumb" style="box-shadow: none;">
                        <i class="fas fa-plus add-pic-icon"></i>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <input type="file" id="add-gal-input" name="gallery" style="display: none;" multiple>
    </div>
    <!-- ================================ FIN Edit Gallery ============================== -->
<style>

</style>
    <script type="text/javascript">
        (function() {
        var bar = $('.bar');
        var percent = $('.percent');
           
        $('#imgup').ajaxForm({
            beforeSend: function() {
                console.log("beforeSend");
                var percentVal = '0%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            success: function() {
                var percentVal = '100%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            complete: function(xhr) {
                console.log(xhr.responseText);
                //status.html(xhr.responseText);
            }
        }); 

        })();       
    </script>
