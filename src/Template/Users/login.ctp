<?php $this->assign('title','TCSI - Authentification'); ?>
<div class="col-md-3" style="margin-left: auto;margin-right: auto; margin-top: 10%;">
	<div class="content">
            <form class="login-form">
                <div class="alert alert-danger" id="loginerror">
                    <?= __("Nom d'utilisateur ou Mot de passe incorrect") ?>
                </div>
                <div class="form-element" style="margin-bottom: 10px;">
                    <input type="email" name="primaryemail" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" style="width: 95%; height: 40px;" placeholder="E-mail" required>
                </div>
                <div class="form-element">
                    <input type="password" name="password" style="width: 95%;height: 40px;" placeholder="Password">
                </div>
                <div class="form-element" style="text-align: center;font-size: 12px;margin-bottom: 10px;">
                    <a href="#" class="lnk"><?= __('Mot de passe Oublié?') ?></a>
                </div>
                <div id="loading" style="text-align: center; display: none;">
                    <?= $this->Html->image('loading.gif',['style'=>'width: 30px;']);?>
                </div>
                <div class="form-element" style="margin-bottom: 20px;">
                    <button type="submit" class="submit-btn" style="width: 95%;margin-left: 0;"><?= __('Se Connecter') ?></button>
                </div>
            </form>
        </div>
</div>