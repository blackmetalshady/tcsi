<?php $this->assign('title','Reinitialisation du Mot de passe'); ?>
<div class="row">
	<div class="col-md-4" style="margin-left: auto;margin-right: auto;">
		<form method="post" accept="/users/resetpassword">
			<div class="form-element">
				<input type="text" name="primaryemail" placeholder="email" value="<?= $this->request->session()->read('Usermail');?>" style="width: 100%; height: 40px; font-size: 20px;" disabled>
			</div>
			<div class="form-element">
				<input type="password" name="password" style="width: 100%; height: 40px;font-size: 20px;" placeholder="Nouveau Mot de Passe">
			</div>
			<div class="form-element">
				<input type="password" name="confirm_password" style="width: 100%; height: 40px;font-size: 20px;" placeholder="Retaper le Nouveau Mot de Passe">
			</div>
			<div class="form-element">
				<button type="submit" class="submit-btn">Envoyer</button>
			</div>
		</form>
		
	</div>
</div>