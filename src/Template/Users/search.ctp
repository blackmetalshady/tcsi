<?php $this->assign('title','Recherche avancée'); ?>
<div class="col-md-9 profile-container">
    <?php if (empty($search_res->toArray())): ?>
        <h2 style="text-align: center;color: #fff; font-weight: bold; margin-top: 90px;">Aucun résultat n'a été trouvé, Veuillez revenir plus tard</h2>
    <?php else: ?>
        <?php foreach ($search_res as $res): ?>
        	<div class="row" style="background-color: #000;padding-left: 38px;">
            <div class="col-md-1" style="padding: 0; max-width: 270px;">
                <div class="img-container">
                	<a href="/users/profile/<?= $res['id'] ?>">
                    <img src="<?= (!empty($res['profilepic']))? $res['profilepic'] : '/img/default.jpg'; ?>" style="max-width: 150px" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md" style="color: #fff; margin-top: 8px;">
                <h2 style="margin: 0;">
                	<b>
                		<a href="/users/profile/<?= $res['id'] ?>" class="lnk">
                		<?= (!empty($res['company']))? ucwords($res['company']) : ucwords($res['firstname']." ".$res['lastname']); ?>	
                		</a>	
                	</b>
                </h2>
                <div style="font-size: 20px; font-weight: bold; margin-bottom: 10px;">
                	<?= $res['category']['name']?>
                </div>
                <div>
                	<?= $this->Text->truncate(ucfirst($res['presentation']), 300, 
                		[
        				    'ellipsis' => '...',
        				    'exact' => true
        				]) ?>
                </div>
            </div>
        </div>
        <?php endforeach ?>
    	<div class="col-md-12 text-center">
    	  <ul class="pagination">
    		<?php $this->Paginator->templates([
    		    'number' => '<li class="page-item"><a href="{{url}}">{{text}}</a></li>'
    		]); ?>
    		<?= $this->Paginator->prev('« Précédent') ?>
    		<!-- <?= $this->Paginator->counter() ?> -->
    		<?= $this->Paginator->numbers(); ?>
    		<?= $this->Paginator->next('Suivant »') ?>
    		</ul>
    	</div>
    <?php endif ?>
</div>