<?php $this->assign('title','TCSI - Parametres'); ?>
<?php //print_r($user);die(); ?>
<div class="col-md-9">
	<!-- ================================ Edit +++++++++++++++++++ ============================== -->
    <div>
        <div class="content">
            <form id="edit-form">
                <div class="form-element">
                    <label for="city" class="required"><?= __('Nom') ?></label>
                    <input type="text" name="city" value="<?= $user['city']?>" required>
                </div>
                <div class="form-element">
                    <label for="address"><?= __('Prenom') ?></label>
                    <textarea name="address" value="<?= $user['address']?>" rows="4"></textarea>
                </div>
                <div class="form-element">
                    <label for="zipcode"><?= __('Code Postal') ?></label>
                    <input type="number" value="<?= $user['zipcode']?>" name="zipcode">
                </div>
                <div class="form-element">
                    <label for="phone" class="required"><?= __('Tél.') ?></label>
                    <input type="text" name="phone" value="<?= $user['phone']?>" required>
                </div>
                <div class="form-element">
                    <label for="fax">Fax</label>
                    <input type="text" value="<?= $user['fax']?>" name="fax">
                </div>
                <div class="form-element">
                    <label for="gsm">GSM</label>
                    <input type="text" value="<?= $user['gsm']?>" name="gsm">
                </div>
                <div class="form-element">
                    <label for="website">Site Web</label>
                    <input type="text" value="<?= $user['website']?>" name="website">
                </div>
                <div class="form-element">
                    <label for="facebook">Facebook</label>
                    <input type="text" value="<?= $user['facebook']?>" name="facebook">
                </div>
                <div class="form-element">
                    <label for="linkedin">Linkedin</label>
                    <input type="text" value="<?= $user['linkedin']?>" name="linkedin">
                </div>
                <div class="form-element">
                    <label for="twitter">Twitter</label>
                    <input type="text" value="<?= $user['twitter']?>" name="twitter">
                </div>
                <div class="form-element">
                    <label for="instagram">Instagram</label>
                    <input type="text" value="<?= $user['instagram']?>" name="instagram">
                </div>
                <div class="form-element">
                    <label for="pinterest">Pinterest</label>
                    <input type="text" value="<?= $user['pinterest']?>" name="pinterest">
                </div>
                <br/>
                <div class="form-element">
                    <button type="submit" class="submit-btn"><?=__("Mettre a jour") ?></button>
                </div>
            </form>
            <div class="loader-container">
                <?= $this->Html->image('loading.gif',['class'=>'loader']);?>
                <p>Veuillez patientez ...</p>
            </div>
        </div>
    </div>
    <!-- ================================ FIN Edit ============================== -->
</div>