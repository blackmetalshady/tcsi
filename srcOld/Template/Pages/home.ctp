<?php $this->assign('title','The Concept Store International'); ?>
<div class = "main">
    <?= $this->Html->image('logo.jpg', ['class'=>'logo']);?>
    <p class="txt">
        Vous êtes créateur de mode, de bijoux, joaillier, d’accessoires, parfums, cosmétiques, artiste peintre, photographe, sculpteur, designer de meubles ou d’objets de décoration, produits naturels et bio, presse écrite, presse en ligne <a href="http://www.theconceptstoreinternational.com">www.theconceptstoreinternational.com</a>, vous propose plus qu’un annuaire, un site inédit, qui vous permet de rentrer directement en contact avec tous les concepts store &amp; les galeries à travers le monde présent dans 193 pays. Lors de votre inscription avec toutes vos informations et vos photos, ainsi que les mises à jour de votre page, celle-ci sera envoyé systématiquement à toutes les personnes inscrites sur notre site.
    </p>
    <p class="txt">
        You are a creator of fashion, accessories, jewelry, a jeweler, perfumes, cosmetics, artist, painter, photographer, sculptor, or designer of furniture or decorative items, natural and organic products <a href="http://www.theconceptstoreinternational.com">www.theconceptstoreinternational.com</a> proposes more than a directory, an unprecedented website which will allow you to connect directly with all concept stores and galleries throughout the world in 193 countries. Once you have enrolled including your details and photos and your page is updated, it will then be systematically sent to all of the people signed up on our website.
    </p>
    <p class="txt">
        Si Vd. es diseñador de moda, diseñador de joyas, accesorios, perfumes, cosméticos, pintor, fotógrafo, escultor, diseñador de muebles u objetos decorativos, productos naturales y orgánicos, prensa escrita, o prensa digital <a href="http://www.theconceptstoreinternational.com">www.theconceptstoreinternational.com</a> le ofrece más que un carné de direcciones, un sitio inédito, que le permite entrar directamente en contacto con todas las tiendas “concept store” y galerías de todo el mundo en 193 países. Al registrarse con todas sus informaciones y fotos, así como las actualizaciones de su página se enviarán sistemáticamente a todas las personas registradas en nuestro sitio web.
    </p>
    <div class="buttons">
        <button class="register_open">INSCRIPTION</button>
        <button class="register-press_open">INSCRIPTION PRESS</button>
    </div>
    <!-- ================================ INSCRIPTION ============================== -->
    <div id="register" style="display: none;">
        <div class="close-header" style="text-align: right;">
            <i class="fa fa-times register_close"></i>
        </div>
        <div class="content">
            <form id="register-form">
                <div style="padding-left: 5%;margin-bottom: 10px;">
                    <input type="radio" class="usertype" name="type" value="artist" checked><span style="font-size: 20px; margin-left: 10px;">Personnel</span>
                    <input type="radio" class="usertype" name="type" value="company" style="margin-left: 10px;"><span style="font-size: 20px; margin-left: 10px;">Société</span>
                </div>
                <div class="form-element company" style="display: none;">
                    <label for="company" class="required">Nom de la Société</label>
                    <input type="text" name="company" required>
                </div>
                <div class="form-element artist">
                    <label for="lastname" class="required">Nom</label>
                    <input type="text" name="lastname" required>
                </div>
                <div class="form-element artist">
                    <label for="firstname" class="required">Prénom</label>
                    <input type="text" name="firstname" required>
                </div>
                <div class="form-element">
                    <label for="presentation" class="required">Présentation</label>
                    <textarea name="presentation" rows="5" required></textarea>
                </div>
                <div class="form-element">
                    <label for="country" class="required">Pays</label>
                    <input type="text" name="country" required>
                </div>
                <div class="form-element">
                    <label for="city" class="required">Ville</label>
                    <input type="text" name="city" required>
                </div>
                <div class="form-element">
                    <label for="address">Adresse</label>
                    <textarea name="address" rows="4"></textarea>
                </div>
                <div class="form-element">
                    <label for="zipcode">Code Postal</label>
                    <input type="number" name="zipcode">
                </div>
                <div class="form-element">
                    <label for="phone" class="required">Tél</label>
                    <input type="text" name="phone" required>
                </div>
                <div class="form-element">
                    <label for="fax">Fax</label>
                    <input type="text" name="fax">
                </div>
                <div class="form-element">
                    <label for="gsm">GSM</label>
                    <input type="text" name="gsm">
                </div>
                <div class="form-element">
                    <label for="primaryemail" class="required">E-mail</label>
                    <input type="email" name="primaryemail" class="email" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" placeholder="Principal" required>
                    <input type="email" name="secondaryemail" class="email" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" placeholder="Secondaire">
                </div>
                <div class="form-element">
                    <label for="website">Site Web</label>
                    <input type="text" name="website">
                </div>
                <div class="form-element">
                    <label for="facebook">Facebook</label>
                    <input type="text" name="facebook">
                </div>
                <div class="form-element">
                    <label for="linkedin">Linkedin</label>
                    <input type="text" name="linkedin">
                </div>
                <div class="form-element">
                    <label for="twitter">Twitter</label>
                    <input type="text" name="twitter">
                </div>
                <div class="form-element">
                    <label for="instagram">Instagram</label>
                    <input type="text" name="instagram">
                </div>
                <div class="form-element">
                    <label for="pinterest">Pinterest</label>
                    <input type="text" name="pinterest">
                </div>
                <div class="form-element">
                    <button type="submit" class="submit-btn">S'inscrire</button>
                </div>
            </form>
            <div class="loader-container">
                <?= $this->Html->image('loading.gif',['class'=>'loader']);?>
                <p>Veuillez patientez ...</p>
            </div>
        </div>
    </div>
    <!-- ================================ FIN INSCRIPTION ============================== -->
    <!-- ================================ INSCRIPTION PRESS ============================== -->
    <style>

      #result {
        margin-top: 10px;
        width: 900px;
      }

      #result-data {
        display: block;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        word-wrap: break-word;
      }
    </style>
    <div id="register-press" style="display: none;">
        <div class="close-header" style="text-align: right;">
            <i class="fa fa-times register-press_close"></i>
        </div>
        <div class="content">
            <form id="registerpress-form" enctype="multipart/form-data">
                <div class="form-element">
                    <div class="image-editor">
                        <i class="fa fa-pencil edit-img"></i>
                        <input type="file" accept="image/*" id="profile-pic" class="cropit-image-input" style="visibility: hidden;">
                        <!-- <input type="file" class="cropit-image-input"> -->
                        <div class="cropit-preview">
                        </div>
                        <div style="margin-top: 10px; display: none;" class="tools-container">
                            <i class="fa fa-image"></i><input type="range" class="cropit-image-zoom-input"><i class="fa fa-image" style="font-size: 25px;"></i>
                            <img src="/img/check.png" class="validate-img">
                        </div>
                        
                    </div>
                    <!--<div class="img-content">
                        <i class="fa fa-pencil edit-img"></i>
                    </div>
                    <div style="text-align: center; padding: 10px;">Votre logo</div>-->
                    <!-- <input type="file" name="profile-pic" accept="image/*" id="profile-pic" class="cropit-image-input" style="visibility: hidden;"> -->
                </div>
                <!--<div style="padding-left: 5%;margin-bottom: 10px;">
                    <input type="radio" class="usertype" name="type" value="artist" ><span style="font-size: 18px;"> Personnel </span>
                    <input type="radio" class="usertype" name="type" value="company" style="margin-left: 15px;"><span style="font-size: 18px; margin-left: 10px;" checked>Société</span>
                </div>-->
                <input type="hidden" name="profilepic" class="hidden-image-data" />
                <div class="form-element company">
                    <label for="company" class="required">Nom de la Société</label>
                    <input type="text" name="company">
                </div>
                <!--<div class="form-element artist">
                    <label for="lastname" class="required">Nom</label>
                    <input type="text" name="lastname" required>
                </div>
                <div class="form-element artist">
                    <label for="firstname" class="required">Prénom</label>
                    <input type="text" name="firstname" required>
                </div>-->
                <div class="form-element">
                    <label for="presentation" class="required">Présentation</label>
                    <textarea name="presentation" rows="5" required></textarea>
                </div>
                <div class="form-element">
                    <label for="country" class="required">Pays</label>
                    <input type="text" name="country" required>
                </div>
                <div class="form-element">
                    <label for="city" class="required">Ville</label>
                    <input type="text" name="city" required>
                </div>
                <div class="form-element">
                    <label for="address">Adresse</label>
                    <textarea name="address" rows="4"></textarea>
                </div>
                <div class="form-element">
                    <label for="zipcode">Code Postal</label>
                    <input type="number" name="zipcode">
                </div>
                <div class="form-element">
                    <label for="phone" class="required">Tél.</label>
                    <input type="text" name="phone" required>
                </div>
                <div class="form-element">
                    <label for="fax">Fax</label>
                    <input type="text" name="fax">
                </div>
                <div class="form-element">
                    <label for="gsm">GSM</label>
                    <input type="text" name="gsm">
                </div>
                <div class="form-element">
                    <label for="primaryemail" class="required">E-mail</label>
                    <input type="email" name="primaryemail" class="email" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" placeholder="Principal" required>
                    <input type="email" name="secondaryemail" class="email" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" placeholder="Secondaire">
                </div>
                <div class="form-element">
                    <label for="website">Site Web</label>
                    <input type="text" name="website">
                </div>
                <div class="form-element">
                    <label for="facebook">Facebook</label>
                    <input type="text" name="facebook">
                </div>
                <div class="form-element">
                    <label for="linkedin">Linkedin</label>
                    <input type="text" name="linkedin">
                </div>
                <div class="form-element">
                    <label for="twitter">Twitter</label>
                    <input type="text" name="twitter">
                </div>
                <div class="form-element">
                    <label for="instagram">Instagram</label>
                    <input type="text" name="instagram">
                </div>
                <div class="form-element">
                    <label for="pinterest">Pinterest</label>
                    <input type="text" name="pinterest">
                </div>
                <br/>
                <div class="gallery-text">Votre Gallerie</div>
                <div class="separator"></div>
                <div class="form-element">
                    <?php for ($i=0; $i < 5; $i++):?>
                        <div class="up-block" style="padding: 10px;">
                            <i class="fa fa-camera" style="vertical-align: sub; font-size: 25px;"></i><span><input type="file" name="gallerie[]" class="gallerie-files" style="margin-left: 15px;"></span>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="form-element">
                    <button type="submit" class="submit-btn">S'inscrire</button>
                </div>
            </form>
            <div class="loaderpress-container">
                <?= $this->Html->image('loading.gif',['class'=>'loader']);?>
                <p>Veuillez patientez ...</p>
            </div>
        </div>
    </div>
    <!-- ================================ FIN INSCRIPTION ============================== -->
</div>
<div class="underconstruction">
    <div>Site en Construction...</div>
    <div>Under Construction...</div>
    <div>Sitio en construcción</div>
</div>