<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('type')
            ->maxLength('type', 10)
            ->allowEmpty('type');

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 255)
            ->allowEmpty('firstname');

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 255)
            ->allowEmpty('lastname');

        $validator
            ->scalar('company')
            ->maxLength('company', 255)
            ->allowEmpty('company');

        $validator
            ->scalar('presentation')
            ->requirePresence('presentation', 'create')
            ->notEmpty('presentation');

        $validator
            ->scalar('country')
            ->maxLength('country', 50)
            ->requirePresence('country', 'create')
            ->notEmpty('country');

        $validator
            ->scalar('city')
            ->maxLength('city', 50)
            ->requirePresence('city', 'create')
            ->notEmpty('city');

        $validator
            ->scalar('address')
            ->maxLength('address', 255)
            ->allowEmpty('address');

        $validator
            ->integer('zipcode')
            ->allowEmpty('zipcode');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 20)
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');

        $validator
            ->scalar('fax')
            ->maxLength('fax', 20)
            ->allowEmpty('fax');

        $validator
            ->scalar('gsm')
            ->maxLength('gsm', 20)
            ->allowEmpty('gsm');

        $validator
            ->scalar('primaryemail')
            ->maxLength('primaryemail', 255)
            ->requirePresence('primaryemail', 'create')
            ->notEmpty('primaryemail');

        $validator
            ->scalar('secondaryemail')
            ->maxLength('secondaryemail', 255)
            ->allowEmpty('secondaryemail');

        $validator
            ->scalar('website')
            ->maxLength('website', 255)
            ->allowEmpty('website');

        $validator
            ->scalar('facebook')
            ->maxLength('facebook', 255)
            ->allowEmpty('facebook');

        $validator
            ->scalar('twitter')
            ->maxLength('twitter', 255)
            ->allowEmpty('twitter');

        $validator
            ->scalar('instagram')
            ->maxLength('instagram', 255)
            ->allowEmpty('instagram');

        $validator
            ->scalar('pinterest')
            ->maxLength('pinterest', 255)
            ->allowEmpty('pinterest');

        $validator
            ->scalar('profilepic')
            ->allowEmpty('profilepic');

        return $validator;
    }
}
