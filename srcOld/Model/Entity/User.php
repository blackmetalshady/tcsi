<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property string $id
 * @property string $type
 * @property string $firstname
 * @property string $lastname
 * @property string $company
 * @property string $presentation
 * @property string $country
 * @property string $city
 * @property string $address
 * @property int $zipcode
 * @property string $phone
 * @property string $fax
 * @property string $gsm
 * @property string $primaryemail
 * @property string $secondaryemail
 * @property string $website
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $pinterest
 * @property string $profilepic
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type' => true,
        'firstname' => true,
        'lastname' => true,
        'company' => true,
        'presentation' => true,
        'country' => true,
        'city' => true,
        'address' => true,
        'zipcode' => true,
        'phone' => true,
        'fax' => true,
        'gsm' => true,
        'primaryemail' => true,
        'secondaryemail' => true,
        'website' => true,
        'facebook' => true,
        'twitter' => true,
        'instagram' => true,
        'pinterest' => true,
        'profilepic' => true
    ];
}
