<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Users Controller
 *
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['register']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function register(){
        if ($this->request->is('post')) {
            //print_r($this->request->getData()); die();
            $user = $this->Users->newEntity();
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if($usr = $this->Users->save($user)){
                if (isset($this->request->getData()['gallerie']) && !empty($this->request->getData()['gallerie'])) {
                    //////////////////////////////////////// Gallerie Upload
                    $this->loadModel("Pictures");
                    if (!file_exists(WWW_ROOT.'img/gallerie/'.$usr->id) || !is_dir(WWW_ROOT.'img/gallerie/'.$usr->id)) {
                        mkdir(WWW_ROOT.'img/gallerie/'.$usr->id, 0777);
                    }
                    foreach ($this->request->getData()['gallerie'] as $pic) {
                        //print_r($pic); die();
                        if (!empty($pic)) {
                            $ext = strtolower(pathinfo($pic['name'], PATHINFO_EXTENSION));
                            $filename = md5($usr->id.time()).'.'.$ext;
                            $finalname = WWW_ROOT.'img/gallerie/'.$usr->id.'/'.$filename;
                            if(move_uploaded_file($pic['tmp_name'], $finalname)){
                                chmod($finalname, 0766);
                                $photo = $this->Pictures->newEntity();
                                $photo = $this->Pictures->patchEntity($photo, ['user_id'=>$usr->id, 'filename'=>$filename]) ;
                                if (!$this->Pictures->save($photo)) {
                                    print_r($photo); die();
                                }
                            }else{
                                print_r($photo); die();
                            }
                        }
                        

                    }
                    //$this->sendadminemail($this->request->getData(), $usr->id);
                    if ($this->sendemail($user->primaryemail)) {
                        $this->sendadminemail($this->request->getData(), $usr->id);
                    }else{
                        echo "error send email"; die();
                    }
                    echo "success";die();
                }else{
                    if ($this->sendemail($user->primaryemail)) {
                        $this->sendadminemail($this->request->getData(), $usr->id);
                    }else{
                        echo "error send email"; die();
                    }
                    echo "success";die();
                }
            }else{
                echo "Error saving user data\n";
                print_r($user); die();
            }
            //print_r($this->request->getData()); die();
        }else{
            echo "Get Method is not allowed";die();
        }

        die();
    }
    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
