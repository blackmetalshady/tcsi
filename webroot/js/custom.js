$(document).ready(function() {

  tinymce.init({ 
    selector:'#tinyarea',
    menubar: false,
    plugins: ["lists", "link"],
    toolbar: [
      'undo redo | bold italic | link | alignleft aligncenter alignright | bullist numlist'
    ] 
  });
  $('.home-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });

  $('.profile_carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        autoWidth: true,
        autoHeight: true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });
  $('.image-editor').cropit();
  $('.edit-image-editor').cropit();
  $('.validate-img').on('click', function(){
      var imageData = $('.image-editor').cropit('export');
      $('.hidden-image-data').val(imageData);
  });
  $('.edit-validate-img').on('click', function(){
      var imageData = $('.edit-image-editor').cropit('export');
      $('.edit-hidden-image-data').val(imageData);
  })
  // Initialize the plugin
  $.fn.popup.defaults.transition = 'all 0.3s';
  $('#regiser').popup();
  $('#register-press').popup();
  $('#login').popup();
  $('#mentionlegal').popup();
  $('#edit').popup();
  $('#change_pass').popup();
  $('#edit_presentation').popup();
  $('#gallery_pic').popup();
  $('#edit_pic').popup({
    onopen: function () {
            $('.edit-img').click();
    }
  });
  $('#slider').popup({
    onopen: function(){
      $('.flexslider').flexslider({
        animation: "fade",
        controlNav: false,
        smoothHeight: true
      });
    }
  });
  //open simple registration popup
  $(".register_open").on('click', function(){
    $('#register').popup();
  });

  $(".register-press_open").on('click', function(){
    $('#register-press').popup();
  });

  $(".login_open").on('click', function(){
    $('#login').popup();
  });

  $(".usertype").on('click', function(){
    if($(this).val() === 'artist'){
    	$(".company").hide();
      $(".company>input").removeAttr('required');
    	$(".artist").show();
      $(".artist>input").prop('required', true);
    }else if($(this).val() === 'company'){
      $(".artist.particular").hide();
      $(".artist.particular>input").removeAttr('required');
    	$(".company").show();
      $(".company>input.req").prop('required', true);
    }else{
      console.log("particulier");
      $(".artist,.company").hide();
      $(".artist.company>input, .artist.company>textarea").removeAttr('required');
      $(".particular").show();
      $(".particular>input").prop('required', true);
      
    }
  });
  //serialize form before submit
  $(document).on('submit',"#register-form", function(e){
  	$(".emailexists").hide();
    //var formData = $("#register-form").serializeArray();
    //console.log(formData);
    if ($("#register-form").find("input[name=password]").val() != $("#register-form").find("input[name=confirm_password]").val()) {
      $("#register-form").find("input[type=password]").css("border-color","red");
    }else{
      $("#register-form").hide();
      $(".loader-container").show();
  	$("#register-form").ajaxSubmit({url: '/users/register', type: 'post', success: function(responseText){
  		//console.log("responseText: "+responseText);
  		$(".loader-container").empty();
  		if (responseText === "success") {
  			$(".loader-container").append("<div class='success'>Bienvenue sur <strong>The Concept Store International</strong>, Nous vous remercions de votre inscription</div><br/><div class='success'>Welcome to <strong>The Concept Store International</strong>, Thank you for your registration</div><br/><div class='success'>Bienvenido al <strong>Concepto de Tienda Internacional</strong>, Gracias por su registro</div><br/><img src='/img/loading.gif' style='margin: auto' />");
        window.setTimeout( function(){
            window.location.replace("/users/profile");;
        }, 3000 );
  		}else if(responseText === "exists"){
        $(".loader-container").hide();
        $(".loader-container").empty();
        $(".loader-container").append("<img src='/img/loading.gif' class='loader' alt=''><p>Veuillez patientez ...</p>")
        $("#register-form").show();
        $(".emailexists").show();
        $("#register-form").find("input[name=primaryemail]").css("border-color","red");
      }else{
  			$(".loader-container").append("<div class='error'>Une erreur est survenue, Veuillez réessayer plutard</div>");
  		}
  		//console.log('saliiina salina');
  	}});
    }//end if
  	e.preventDefault();
  });
  
  $(document).on('submit',"#newsletter-form", function(e){
    $(".newsletterInput").attr("readonly", true);
    $("#newsletter-form").ajaxSubmit({url: '/users/newslettersubscription', type: 'post', success: function(responseText){
      if (responseText === "success") {
        alert("Votre email a été enregistrer avec success");
      }else if(responseText === "exists"){
        alert("L'email que vous avez saisie exists déjà, veuillez choisir un autre");
        $(".newsletterInput").attr("readonly", false);
      }else{
        alert("Une erreur est survenue, Veuillez réessayer plutard");
        $(".newsletterInput").attr("readonly", false);
      }
    }});
    e.preventDefault();
  });
  
  //serialize form before submit
  $("#registerpress-form").on('submit', function(e){
    $("#registerpress-form").find("input[type=password]").css("border-color","");
    //console.log('subscription form')
  	//console.log($("#registerpress-form").serializeArray());
    if ($("#registerpress-form").find("input[name=password]").val() != $("#registerpress-form").find("input[name=confirm_password]").val()) {
      $("#registerpress-form").find("input[type=password]").css("border-color","red");
    }else{
      if ($('#profile-pic').get(0).files.length === 0 ) {
            $("#registerpress-form").hide();
            $(".loaderpress-container").show();
            $("#registerpress-form").ajaxSubmit({url: '/users/register', type: 'post', success: function(responseText){
              //console.log("responseText: "+responseText);
              
              if (responseText === "success") {
                $(".loaderpress-container").empty();
                $(".loaderpress-container").append("<div class='success'>Bienvenue sur <strong>The Concept Store International</strong>, Nous vous remercions de votre inscription</div><br/><div class='success'>Welcome to <strong>The Concept Store International</strong>, Thank you for your registration</div><br/><div class='success'>Bienvenido al <strong>Concepto de Tienda Internacional</strong>, Gracias por su registro</div><img src='/img/loading.gif' style='margin: auto' />");
                window.setTimeout( function(){
                    window.location.replace("/users/profile");;
                }, 3000 );
              }else if(responseText === "exists"){
                $(".loaderpress-container").hide();
                $("#registerpress-form").show();
                $(".pressemailexists").show();
                $("#registerpress-form").find("input[name=primaryemail]").css("border-color","red");
                $('.content').animate({
                                      scrollTop: $("#registerpress-form").offset().top},
                                      'slow');
              }else{
                $(".loaderpress-container").empty();
                $(".loaderpress-container").append("<div class='error'>Une erreur est survenue, Veuillez réessayer plutard</div>");
              }
            }});
      }else{
        if (!$(".hidden-image-data").val()) {
          if (confirm("Vous avez changer votre logo mais vous ne l'avez pas valider, voulez vous continuer sans la photo ?")) {
            $("#registerpress-form").hide();
            $(".loaderpress-container").show();
            $("#registerpress-form").ajaxSubmit({url: '/users/register', type: 'post', success: function(responseText){
              //console.log("responseText: "+responseText);
              
              if (responseText === "success") {
                $(".loaderpress-container").empty();
                $(".loaderpress-container").append("<div class='success'>Bienvenue sur <strong>The Concept Store International</strong>, Nous vous remercions de votre inscription</div><br/><div class='success'>Welcome to <strong>The Concept Store International</strong>, Thank you for your registration</div><br/><div class='success'>Bienvenido al <strong>Concepto de Tienda Internacional</strong>, Gracias por su registro</div><img src='/img/loading.gif' style='margin: auto' />");
                window.setTimeout( function(){
                    window.location.replace("/users/profile");;
                }, 3000 );
              }else if(responseText === "exists"){
                $("#registerpress-form").show();
                $(".pressemailexists").show();
                $("#registerpress-form").find("input[name=primaryemail]").css("border-color","red");
              }else{
                $(".loaderpress-container").empty();
                $(".loaderpress-container").append("<div class='error'>Une erreur est survenue, Veuillez réessayer plutard</div>");
              }
            }});
          }
        }else{
          $("#registerpress-form").hide();
            $(".loaderpress-container").show();
            $("#registerpress-form").ajaxSubmit({url: '/users/register', type: 'post', success: function(responseText){
              $(".loaderpress-container").empty();
              if (responseText === "success") {
                $(".loaderpress-container").append("<div class='success'>Bienvenue sur <strong>The Concept Store International</strong>, Nous vous remercions de votre inscription</div><br/><div class='success'>Welcome to <strong>The Concept Store International</strong>, Thank you for your registration</div><br/><div class='success'>Bienvenido al <strong>Concepto de Tienda Internacional</strong>, Gracias por su registro</div><img src='/img/loading.gif' style='margin: auto' />");
                window.setTimeout( function(){
                    window.location.replace("/users/profile");;
                }, 3000 );
              }else if(responseText === "exists"){
                $("#registerpress-form").show();
                $(".emailexists").show();
                $("#registerpress-form").find("input[name=primaryemail]").css("border-color","red");
              }else{
                $(".loaderpress-container").append("<div class='error'>Une erreur est survenue, Veuillez réessayer plutard</div>");
              }
            }});
        }
      }//
    }

    
  	
  	e.preventDefault();
  });


  $("form.login-form").on('submit', function(e){
    $("#loading").show();
    $("form.login-form").ajaxSubmit({url: '/users/login', type: 'post', success: function(responseText){
        if (responseText === "success") {
          window.location.replace("/users/profile");
        }else if(responseText === "reset"){
          window.location.replace("/users/resetpassword");
        }else{
          $("#loading").hide();
          $("#loginerror").show();
        }
      }
    });
    e.preventDefault();
  });
/****************** Change Password ***********************/
$("button.change-pass").on('click', function(e){
    $("#changepass-err").hide();
    $("#change_pass").find("#loading").show();

    var oldpass = $("#change_pass").find("#old_pass").val();
    var newpass = $("#change_pass").find("#new_pass").val();
    var confirmnewpass = $("#change_pass").find("#confirm_new_pass").val();
    if (!oldpass || !newpass || !confirmnewpass) {
        $("#change_pass").find("#loading").hide();
        $("#changepass-err").text("Veuillez remplir tous les champs");
        $("#changepass-err").show();
        return false;
    }

    if (newpass != confirmnewpass) {
        $("#change_pass").find("#loading").hide();
        $("#changepass-err").text("le nouveau mot de passe et sa confirmation doivent etre les memes");
        $("#changepass-err").show();
        return false;
    }

    $.post("/users/changepassword", {'oldpass': oldpass, 'newpass': newpass, 'confirmnewpass': confirmnewpass})
      .done(function(data){
        console.log(data);
        if (data == "success") {
          $("#changepass-success").show();
          window.setTimeout( function(){
                 window.location.replace("/users/login");;
             }, 3000 );
          
        }else if(data == "oldpassnomatch"){
          $("#changepass-err").text("'Ancien Mot de Passe' ne correspond pas a votre mot de passe actuel");
          $("#changepass-err").show();
          $("#change_pass").find("#loading").hide();
        }else if(data == "lengtherror"){
          $("#changepass-err").text("le mot de passe doit avoir au moins 6 characteres");
          $("#changepass-err").show();
          $("#change_pass").find("#loading").hide();
        }else if(data == "newpassnomatch"){
          $("#changepass-err").text("le nouveau mot de passe et sa confirmation doivent etre les memes");
          $("#changepass-err").show();
          $("#change_pass").find("#loading").hide();
        }else{
          $("#changepass-err").text("Une erreur est survenue, Veuillez réessayer plutard");
          $("#changepass-err").show();
          $("#change_pass").find("#loading").hide();
        }
      })
        .fail(function() {
        alert( "error" );
      })
    /*$("#change-pass-form").ajaxSubmit({url: '/users/changepassword', type: 'post', success: function(responseText){
        if (responseText === "success") {
          setTimeout(location.reload(), 3000);
        }else if(responseText === "oldpassincorrect"){
          $("#changepass-err").text("'Ancien Mot de Passe' ne correspond pas a votre mot de passe actuel");
          $("#changepass-err").show();
          //window.location.replace("/users/resetpassword");
        }else{
          $("#change-pass-form #loading").hide();
          //$("#loginerror").show();
        }
      }
    });*/
    e.preventDefault();
  });
/****************** End Change Password ***********************/
$(document).on('submit',"#contact-form", function(e){
  $("#contact-form :input").attr("readonly", true);
  $("#loader").show();
  $("#contact-form").ajaxSubmit({url: '/messages/add', type: 'post', success: function(responseText){
    if (responseText === "success") {
      $("#contact-form :input").attr("readonly", false);
      $("#loader").hide();
      alert("message envoyer avec success");
      $("#contact-form")[0].reset();
    }else{
      $("#contact-form :input").attr("readonly", false);
      $("#loader").hide();
      alert("une erreur est survenue, veuillez reessayer plutard");
    }
  }});
  e.preventDefault();
});
  $(document).on('change', '#profile-pic', function(){
    $(".hidden-image-data").val('');
    $('.tools-container').show();
  });

//******** Update Profile Picture **************/
$(document).on('click', '.submit-btn.profilepic', function(){

  if (!$(".hidden-image-data").val()){
    $(".alert-pic-empty").show();
    $(".validate-img").css("transition","all .5s ease-in-out;");
    $(".validate-img").css("transform","scale(1.5)");
    setTimeout(function(){
      $(".validate-img").css("transform","");
    },200);
    return false;
  };
  $(".alert-pic-empty").hide();
  $("#edit_pic #loading").show();
  $.post("/users/edit", {profilepic: $(".hidden-image-data").val()})
    .done(function(data){
        if (data == "success") {
          location.reload();
        }else{
          alert("Error update");
        }
      })
        .fail(function() {
        alert( "error" );
      })
});

//*********************** Update Presentation ****************************//
  $(document).on('click', '.submit-btn.presentation', function(){
      $("#edit_presentation #loading").show();
      $.post("/users/edit", {presentation: tinymce.get('tinyarea').getContent()})
        .done(function(data){
            if (data == "success") {
              location.reload();
            }else{
              alert("Error update");
            }
          })
          .fail(function() {
          alert( "error" );
        })
  })
//****************** Search ********************//
$(document).on('mouseover','.search-btn', function(){
  $(this).find("div").show();
});
$(document).on('mouseleave','.search-btn', function(){
  $(this).find("div").hide();
});
$(document).on("change",".search", function(){
  if ($("#search-by-count").val() || $("#search-by-cat").val()) {
    let url = "/search?";
    if ($("#search-by-count").val()) url += "country_id="+$("#search-by-count").val();
    if ($("#search-by-count").val() && $("#search-by-cat").val()) url +="&";
    if ($("#search-by-cat").val()) url += "category_id="+$("#search-by-cat").val();
    window.location.replace(url);
  }
})

//***********************End Search *****************************//
//********************** Add Gallery *************************//
$(document).on('click','#add-gal, .add-pic-icon',function(){
    $('#add-gal-input').click();
});
$(document).on('change','#add-gal-input', function(){

  var fd = new FormData();
  $.each($(this)[0].files, function( index, element ){
    fd.append(index, element);
  })

    $.ajax({
        url: '/medias/uploadGallery',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function(XMLHttpRequest){
          $("#add-gal").children().hide();
          $(".grid-container .loader-container").show();
        },
        success: function(response){
            $(".grid-container .loader-container").hide();

            if ($(".grid-container").length) {
              $(".grid-container .grid-item:last").before('<div class="grid-item" id="'+response['pid']+'"><div class="thumbnail"><img src="/img/gallerie/'+response['uid']+'/'+response['file']+'" /><i class="fas fa-times del-pic" data-pic="'+response['file']+'"></i></div></div>')
            }else{
              $("#add-gal").remove();
              $(".grid-container .loader-container").hide();
              $("#gallery_pic").append('<div class="grid-container">'
                                          +'<div class="grid-item" id="'+response['pid']+'">'
                                            +'<div class="thumbnail">'
                                              +'<img src="/img/gallerie/'+response['uid']+'/'+response['file']+'" />'
                                                +' <i class="fas fa-times del-pic" data-pic="'+response['file']+'"></i>'
                                            +'</div>'
                                          +'</div>'
                                          +'<div class="grid-item">'
                                            +'<div class="add-thumb">'
                                              +'<i class="fas fa-plus add-pic-icon"></i>'
                                            +'</div>'
                                          +'</div>'
                                        +'</div>')
            }
        }
    });
})
//************************* End Add Gallery ********************//
//******************* Delete from Gallery ****************//
$(document).on('click','.del-pic', function(){
  var filename = $(this).data('pic');
  $.post('/medias/delete',{filename: filename})
    .done(function(data){
      
      data = $.parseJSON(data);
      if (data.status == 'success') {
        $("#"+data.pid).remove();
      }else{
        alert("Une Erreur est survenue");
      }
    })
})
//********************************************************//
$(document).on('submit','#edit-form', function(e){
  $('#edit-form').find('.loader-container').show();
  $("#edit-form").ajaxSubmit({url: '/users/edit', type: 'post', success: function(responseText){
    if (responseText == "exists"){
      $('#edit-form').find('#email-exists').show();
      $('#edit-form').find('.loader-container').hide();
      return false;
    }
    if (responseText == "success") location.reload();
  }});
  
  e.preventDefault();
});

  $('.image-editor').mouseover(function(){
  	$(".edit-img").css('visibility', 'visible');
  });
  $('.image-editor').mouseout(function(){
  	$(".edit-img").css('visibility', 'hidden');
  });

  $(".edit-img").on('click', function(){
  	$("#profile-pic").click();
  });

  $(".newsletterTxt").click(function(){
    $(".newsletterInput").focus();
  })
});
